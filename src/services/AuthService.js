/**
 * Beanworks 2017
 *
 * @flow
 */

import axios from 'axios';

const URL_LOGIN = `${ process.env.REACT_APP_API_URL }/auth/login`;
const URL_LOGOUT = `${ process.env.REACT_APP_API_URL }/auth/logout`;

type UserNameAndPassword = {
    email: string,
    password: string,
};

const AuthService = {
    instantiateAxios(session: Object) {
        axios.defaults.headers.common['Http-Authorization'] = `Bearer ${session.access_token}`;
        axios.defaults.headers.common[`X-PodNumber-${session.podNumber}`] = true;
        axios.defaults.params = {};
        axios.defaults.params['rouid'] = session.rootOrgUnit.id;
    },

    login: function({ username, password }: UserNameAndPassword) {
        return axios.post(URL_LOGIN, {
            username: username,
            password: password,
        });
    },

    logout: function() {
        return axios.delete(URL_LOGOUT);
    },

};

export default AuthService;
