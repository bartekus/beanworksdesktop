/**
 * Beanworks 2017
 *
 * @flow
 */

import React from 'react';
import PropTypes from 'prop-types';

const Errors = (props) => {
    const { error, errors } = props;
    if (errors) {
        return (
            <div>
                <ul>
                    { errors.map(error => (
                        <li key={ error.time }>{ error.body }</li>
                    )) }
                </ul>
            </div>
        )
    } else if (error) {
        return (<p>{ error }</p>)
    }
};

Errors.propTypes = {
    errors: PropTypes.arrayOf(
        PropTypes.shape({
            body: PropTypes.string,
            time: PropTypes.date,
        })
    )
};

export default Errors;
