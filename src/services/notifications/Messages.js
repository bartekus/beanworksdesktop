/**
 * Beanworks 2017
 *
 * @flow
 */

import React from 'react';
import PropTypes from 'prop-types';

const Messages = (props) => {
    const { message, messages } = props;
    if (messages) {
        return (
            <div>
                <ul>
                    <li key={ messages.time }>{ messages.body }</li>
                </ul>
            </div>
        )
    } else if (message) {
        return(<p>{ message }</p>);
    }
};

Messages.propTypes = {
    messages: PropTypes.arrayOf(
        PropTypes.shape({
            body: PropTypes.string,
            time: PropTypes.date

        })
    )
};

export default Messages;
