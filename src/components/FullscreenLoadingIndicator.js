import React from 'react';
import LoadingIndicator from './LoadingIndicator';

export default class FullscreenLoadingIndicator extends React.Component {
    render() {
        return (
            <div
                style={{
                    flex: 1,
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
            >
                <LoadingIndicator />
            </div>
        );
    }
}
