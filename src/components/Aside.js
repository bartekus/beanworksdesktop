/**
 * Beanworks 2017
 *
 * @flow
 */

import React, { Component } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';

class Aside extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            activeTab: '2'
        };
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    render() {
        return (
            <aside className="aside-menu">
                <Nav tabs>
                    <NavItem>
                        <NavLink className={ classnames({ active: this.state.activeTab === '1' }) }
                                 onClick={ () => {
                                     this.toggle('1');
                                 } }>
                            <i className="icon-present"></i>
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink className={ classnames({ active: this.state.activeTab === '2' }) }
                                 onClick={ () => {
                                     this.toggle('2');
                                 } }>
                            <i className="icon-bell"></i>
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink className={ classnames({ active: this.state.activeTab === '3' }) }
                                 onClick={ () => {
                                     this.toggle('3');
                                 } }>
                            <i className="icon-speech"></i>
                        </NavLink>
                    </NavItem>
                </Nav>
                <TabContent activeTab={ this.state.activeTab }>

                    <TabPane tabId="1" className="p-3">
                        <div className="message">
                            <div>
                                <small className="text-muted">May 15 2017</small>
                            </div>
                            <div className="text-truncate font-weight-bold">Coming Soon: Report History</div>
                            <small className="text-muted">
                                We’re so excited to release this feature that we wanted to share our excitement with
                                you!
                                You’ll soon be able to create a report and do other tasks within beanworks while the
                                report is being generated.
                                When it is ready, you’ll receive a notification and can return to the report history box
                                to download it.
                                Click the link below for more details!
                            </small>
                            <div>
                                <small className="text-muted">
                                    <a href="https://support.beanworks.com/customer/portal/articles/2599766-release-notes-for-beanworks?b_id=15735#Reporthistory">
                                        Learn more >
                                    </a>
                                </small>
                            </div>
                        </div>
                        <hr/>
                        <div className="message">
                            <div>
                                <small className="text-muted">January 25 2017</small>
                            </div>
                            <div className="text-truncate font-weight-bold">Security Time-Out</div>
                            <small className="text-muted">
                                Hello Bean-counters! Long time no speak!
                                We have a very exciting new security feature, the automatic logging out of users out
                                after 30 minutes of inactivity.
                                This means that if a mouse movement or click has not been made within your Beanworks
                                browser in 30 minutes, you will be logged out to keep you and your data protected.
                                No risk of snoopy neighbours checking how you code your invoices when you are not
                                looking. To find out more, click the link below!
                            </small>
                            <div>
                                <small className="text-muted">
                                    <a href="http://support.beanworks.com/customer/portal/articles/2599766-release-notes-for-beanworks?b_id=15735#Securitytimeout">
                                        Learn more >
                                    </a>
                                </small>
                            </div>
                        </div>
                        <hr/>
                        <div className="message">
                            <div>
                                <small className="text-muted">October 20 2016</small>
                            </div>
                            <div className="text-truncate font-weight-bold">Batch Create User</div>
                            <small className="text-muted">
                                System administrators can now create a batch of new users by copying a table from Excel
                                or Word and pasting it to the ‘Batch Edit User’ tool found in the general settings area.
                                This will decrease the amount of time spent adding new users and increase the amount
                                time you can spend doing other important things,
                                like sitting back and watching invoices practically process themselves in Beanworks!
                                Ahem… to find out more, click the link below!
                            </small>
                            <div>
                                <small className="text-muted">
                                    <a href="http://support.beanworks.com/customer/portal/articles/2599766-release-notes-for-beanworks?b_id=15735#BatchCreateUsert">
                                        Learn more >
                                    </a>
                                </small>
                            </div>
                        </div>
                        <hr/>
                        <div className="message">
                            <div>
                                <small className="text-muted">October 13 2016</small>
                            </div>
                            <div className="text-truncate font-weight-bold">What's New? What's New!</div>
                            <small className="text-muted">
                                Hello! Welcome to the inaugural Beanworks Feature Note™!
                                And do we ever have a great feature for you today!
                                This is a brand new Feature Notes window where you can read all about the latest and
                                greatest Beanworks features!
                                If you're itching to try get yourself up to speed (and who wouldn't be),
                                kindly click “Learn more at our support desk” and it will take you to the detailed
                                release notes that will give more information about your new features!
                                We hope you are as excited about this feature as we are!
                            </small>
                            <div>
                                <small className="text-muted">
                                    <a href="http://support.beanworks.com/customer/portal/articles/2599766-release-notes-for-beanworks?b_id=15735#Whatsnew">
                                        Learn more >
                                    </a>
                                </small>
                            </div>
                        </div>
                        <hr/>
                        <div className="message">
                            <div>
                                <small className="text-muted">August 31 2016</small>
                            </div>
                            <div className="text-truncate font-weight-bold">New User Interface!</div>
                            <small className="text-muted">
                                Every once in awhile, it’s good to try a new look.
                                Here at Beanworks the team has been working for months to bring a new and improved feel
                                to the system which we are excited to share with you.
                            </small>
                            <div>
                                <small className="text-muted">
                                    <a href="http://support.beanworks.com/customer/portal/articles/2599766-release-notes-for-beanworks?b_id=15735#NewUI">
                                        Learn more >
                                    </a>
                                </small>
                            </div>
                        </div>
                    </TabPane>
                    <TabPane tabId="2" className="p-3">
                        <div className="message">
                            <div>
                                <small className="text-muted">February 09 2017</small>
                            </div>
                            <div className="text-truncate font-weight-bold">Vote for us in IOFM's Game Changer Awards!
                            </div>
                            <small className="text-muted">
                                Beanworks has been chosen to compete in the exciting race to name the Institute of
                                Finance & Management's top five financial solutions of 2017
                                - and we need your help to get there! Voting is open until March 31st and each person
                                can vote once daily
                            </small>
                            <div>
                                <small className="text-muted">
                                    <a href="http://iofm.com/conference-spring/vote-game-changers/#vote">
                                        Vote >
                                    </a>
                                </small>
                            </div>
                        </div>
                        <hr/>
                    </TabPane>
                    <TabPane tabId="3">
                        <div className="callout m-0 py-2 text-muted text-center bg-light text-uppercase">
                            <small>Conversations with Beanworks</small>
                        </div>
                        <hr className="transparent mx-3 my-0"/>
                        <div className="callout callout-warning m-0 py-3">
                            <div className="avatar">
                                <img src={ 'img/avatars/1.jpg' } className="img-avatar" alt="julien@beanworks.com"/>
                            </div>
                            <div className="float-right">
                                <div>Julien</div>
                                <small className="text-muted float-right">Nope you are in prod</small>
                            </div>
                        </div>
                        <hr className="mx-3 my-0"/>
                        <div className="callout callout-warning m-0 py-3">
                            <div className="avatar">
                                <img src={ 'img/avatars/1.jpg' } className="img-avatar" alt="julien@beanworks.com"/>
                            </div>
                            <div className="float-right">
                                <div>Julien</div>
                                <small className="text-muted float-right">yes</small>
                            </div>
                        </div>
                        <hr className="mx-3 my-0"/>
                        <div className="callout callout-warning m-0 py-3">
                            <div className="avatar">
                                <img src={ 'img/avatars/2.png' } className="img-avatar" alt="julien@beanworks.com"/>
                            </div>
                            <div className="float-right">
                                <div>Tom</div>
                                <small className="text-muted float-right">Hi Tom, hello tresrt :S</small>
                            </div>
                        </div>
                        <hr className="mx-3 my-0"/>
                        <div className="callout callout-danger m-0 py-3">
                            <div className="avatars-stack mt-2">
                                <div className="avatar avatar-xs">
                                    <img src={ 'img/avatars/2.jpg' } className="img-avatar"
                                         alt="admin@bootstrapmaster.com"/>
                                </div>
                                <div className="avatar avatar-xs">
                                    <img src={ 'img/avatars/3.jpg' } className="img-avatar"
                                         alt="admin@bootstrapmaster.com"/>
                                </div>
                                <div className="avatar avatar-xs">
                                    <img src={ 'img/avatars/4.jpg' } className="img-avatar"
                                         alt="admin@bootstrapmaster.com"/>
                                </div>
                                <div className="avatar avatar-xs">
                                    <img src={ 'img/avatars/5.jpg' } className="img-avatar"
                                         alt="admin@bootstrapmaster.com"/>
                                </div>
                                <div className="avatar avatar-xs">
                                    <img src={ 'img/avatars/6.jpg' } className="img-avatar"
                                         alt="admin@bootstrapmaster.com"/>
                                </div>
                            </div>
                            <div className="float-right">
                                <div>Beanworks</div>
                                <small className="text-muted float-right">UI Release time is fixed</small>
                            </div>
                        </div>
                        <hr className="mx-3 my-0"/>
                    </TabPane>

                </TabContent>
            </aside>
        )
    }
}

export default Aside;
