/**
 * Beanworks 2017
 *
 * @flow
 */

import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <footer className="app-footer">
                <a
                    href="https://beanworks.com"
                    target="_blank"
                    rel="noopener noreferrer"
                > Beanworks
                </a> &copy; 2017
                <span className="float-right">
                    Need help?
                    <a href="http://support.beanworks.com/" target="_blank" rel="noopener noreferrer"> Visit our Support Desk </a>
                     or contact us at
                    <a href="mailto:support@beanworks.com" target="_blank" rel="noopener noreferrer"> support@beanworks.com </a>
                </span>
            </footer>
        )
    }
}

export default Footer;
