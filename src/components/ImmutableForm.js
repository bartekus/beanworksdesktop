/**
 * Beanworks 2017
 *
 * @flow
 */

import React from 'react';
import { reduxForm, Field } from 'redux-form/immutable';
import Messages from '../services/notifications/Messages';
import Errors from '../services/notifications/Errors';
import { Redirect } from 'react-router';
import { Row, Col, CardBlock, Button, InputGroup, InputGroupAddon } from "reactstrap";

import ValidateForm from '../utils/ValidateForm';

const renderField = ({ input, label, type, meta: { touched, error } }) => (
    <div>
        <label>
            { label }
        </label>
        <div>
            <input { ...input } type={ type } placeholder={ label } />
            { touched && error && <span>{ error }</span> }
        </div>
    </div>
);

const ImmutableForm = ({props}) => {
    const {
        handleSubmit,
        pristine,
        reset,
        submitting,
        login: { requesting, successful, messages, errors, redirectTo }
    } = props;

    return (
        <CardBlock className="card-body">
            { successful && <Redirect to={ redirectTo }/> }
            <h1>Login</h1>
            <p className="text-muted">Sign In to your account</p>

            <div className="card-body">
                <form onSubmit={ handleSubmit }>
                    <InputGroup className="mb-3">
                        <InputGroupAddon>
                            <i className="icon-user"></i>
                        </InputGroupAddon>
                        <Field
                            id="username"
                            name="username"
                            type="text"
                            component={renderField}
                            label="username"
                        />
                    </InputGroup>
                    <InputGroup className="mb-4">
                        <InputGroupAddon>
                            <i className="icon-lock"></i>
                        </InputGroupAddon>
                        <Field
                            id="password"
                            name="password"
                            type="password"
                            component={renderField}
                            label="Password"
                        />
                    </InputGroup>

                    <Row>
                        <Col xs="6">
                            <Button
                                action="submit"
                                disabled={ pristine || submitting }
                                color="primary"
                                className="px-4"
                            >
                                Login
                            </Button>
                            <Button
                                action="submit"
                                disabled={ pristine || submitting }
                                color="primary"
                                className="px-4"
                                onClick={ reset }
                            >
                                Clear Values
                            </Button>
                        </Col>
                        <Col xs="6" className="text-right">
                            <Button color="link" className="px-0">
                                Forgot password?
                            </Button>
                        </Col>
                    </Row>
                </form>
            </div>

            <div className="auth-messages">
                { /* As in the signup, we're just using the message and error helpers */ }
                { !requesting && !!errors.length && (
                    <Errors message="Failure to login due to:" errors={ errors }/>
                ) }
                { !requesting && !!messages.length && (
                    <Messages messages={ messages }/>
                ) }
                { requesting && <div>Logging in...</div> }
            </div>
        </CardBlock>
    );
};

export default reduxForm({
    form: 'login', // a unique identifier for this form
    ValidateForm,
})(ImmutableForm);
