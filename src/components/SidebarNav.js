/**
 * Beanworks 2017
 *
 * @flow
 */

export default {
    items: [
        {
            name: 'Beanboard',
            url: '/beanboard',
            icon: 'icon-chart',
            badge: {
                variant: 'info',
                text: null
            }
        },
        {
            name: 'Purchase Orders',
            url: '/beanboard/pos',
            icon: 'icon-list',
            badge: {
                variant: 'info',
                text: null,
                match: {
                    section: 'purchaseOrders',
                    title: 'inProgress'
                }
            },
            children: [
                {
                    name: 'Create',
                    url: '/beanboard/pos/create',
                    badge: {
                        variant: 'info',
                        text: null
                    }
                },
                {
                    name: 'In-Progress',
                    url: '/beanboard/pos/inprogress',
                    badge: {
                        variant: 'info',
                        text: null,
                        match: {
                            section: 'purchaseOrders',
                            title: 'inProgress'
                        }
                    }
                },
                {
                    name: 'Approvals',
                    url: '/beanboard/pos/approvals',
                    badge: {
                        variant: 'info',
                        text: null,
                        match: {
                            section: 'purchaseOrders',
                            title: 'approvals'
                        }
                    }
                },
                {
                    name: 'Committed',
                    url: '/beanboard/pos/committed',
                    badge: {
                        variant: 'info',
                        text: null
                    }
                },
                {
                    name: 'Closed',
                    url: '/beanboard/pos/closed',
                    badge: {
                        variant: 'info',
                        text: null
                    }
                },
            ]
        },
        {
            name: 'Expenses',
            url: '/beanboard/expenses',
            icon: 'icon-book-open',
            badge: {
                variant: 'info',
                text: null,
                match: {
                    section: 'expenses',
                    title: 'createReceipt'
                }
            },
            children: [
                {
                    name: 'Create Receipts',
                    url: '/beanboard/expenses/receipts/arrivals',
                    badge: {
                        variant: 'info',
                        text: null,
                        match: {
                            section: 'expenses',
                            title: 'createReceipt'
                        }
                    }
                },
                {
                    name: 'Create Expenses',
                    url: '/beanboard/expenses/create',
                    badge: {
                        variant: 'info',
                        text: null,
                        match: {
                            section: 'expenses',
                            title: 'createExpense'
                        }
                    }
                },
                {
                    name: 'In-Progress',
                    url: '/beanboard/expenses/inprogress',
                    badge: {
                        variant: 'info',
                        text: null,
                        match: {
                            section: 'expenses',
                            title: 'inProgress'
                        }
                    }
                },
                {
                    name: 'Approvals',
                    url: '/beanboard/expenses/approvals',
                    badge: {
                        variant: 'info',
                        text: null,
                        match: {
                            section: 'expenses',
                            title: 'approvals'
                        }
                    }
                },
                {
                    name: 'Approved',
                    url: '/beanboard/expenses/committed',
                    badge: {
                        variant: 'info',
                        text: null
                    }
                },
            ]
        },
        {
            name: 'Invoices',
            url: '/beanboard/invoices',
            icon: 'icon-doc',
            badge: {
                variant: 'info',
                text: null,
                match: {
                    section: 'invoices',
                    title: 'inProgress'
                }
            },
            children: [
                {
                    name: 'Create',
                    url: '/beanboard/invoices/arrivals',
                    badge: {
                        variant: 'info',
                        text: null,
                        match: {
                            section: 'invoices',
                            title: 'create'
                        }
                    }
                },
                {
                    name: 'In-Progress',
                    url: '/beanboard/invoices/inprogress',
                    badge: {
                        variant: 'info',
                        text: null,
                        match: {
                            section: 'invoices',
                            title: 'inProgress'
                        }
                    }
                },
                {
                    name: 'Approvals',
                    url: '/beanboard/invoices/approvals',
                    badge: {
                        variant: 'info',
                        text: null,
                        match: {
                            section: 'invoices',
                            title: 'approvals'
                        }
                    }
                },
                {
                    name: 'Export',
                    url: '/beanboard/invoices/export',
                    badge: {
                        variant: 'info',
                        text: null,
                        match: {
                            section: 'invoices',
                            title: 'export'
                        }
                    }
                },
                {
                    name: 'Archived',
                    url: '/beanboard/invoices/archived',
                    badge: {
                        variant: 'info',
                        text: null
                    }
                },
            ]
        },
        {
            name: 'Payments',
            url: '/beanboard/payments/inprogress',
            icon: 'icon-credit-card',
            badge: {
                variant: 'info',
                text: null,
                match: {
                    section: 'payments',
                    title: 'inProgress'
                }
            },
            children: [
                {
                    name: 'Create',
                    url: '/beanboard/payments/create',
                    badge: {
                        variant: 'info',
                        text: null,
                        match: {
                            section: 'payments',
                            title: 'create'
                        }
                    }
                },
                {
                    name: 'In-Progress',
                    url: '/beanboard/payments/inprogress',
                    badge: {
                        variant: 'info',
                        text: null,
                        match: {
                            section: 'payments',
                            title: 'inProgress'
                        }
                    }
                },
                {
                    name: 'Approvals',
                    url: '/beanboard/payments/approvals',
                    badge: {
                        variant: 'info',
                        text: null,
                        match: {
                            section: 'payments',
                            title: 'approvals'
                        }
                    }
                },
                {
                    name: 'Released',
                    url: '/beanboard/payments/released',
                    badge: {
                        variant: 'info',
                        text: null,
                        match: {
                            section: 'payments',
                            title: 'releases'
                        }
                    }
                },
                {
                    name: 'Paid',
                    url: '/beanboard/payments/paid',
                    badge: {
                        variant: 'info',
                        text: null,
                        match: {
                            section: 'payments',
                            title: 'paid'
                        }
                    }
                },
                {
                    name: 'Archived',
                    url: '/beanboard/payments/archived',
                    badge: {
                        variant: 'info',
                        text: null
                    }
                },
            ]
        },
    ]
};
