/**
 * Beanworks 2017
 *
 * @flow
 */

import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Badge, Nav, NavItem, NavLink as RsNavLink } from 'reactstrap';
import isExternal from 'is-url-external';
import classNames from 'classnames';
import nav from './SidebarNav';
import SidebarFooter from './SidebarFooter';
import SidebarForm from './SidebarForm';
import SidebarHeader from './SidebarHeader';
import SidebarMinimizer from './SidebarMinimizer';

const codeableItemsCount = {
    'purchaseOrders': { 'inProgress': 9, 'approvals': 0 },
    'invoices': { 'create': 2, 'inProgress': 7, 'approvals': 0, 'export': 22 },
    'expenses': { 'createReceipt': 0, 'createExpense': 1, 'inProgress': 0, 'approvals': 0 },
    'payments': { 'create': 18, 'inProgress': 11, 'approvals': 0, 'releases': 4, 'paid': 2 },
};

class Sidebar extends Component {
    constructor() {
        super();

        this.toggle = this.toggle.bind(this);
        this.state = {
            minimized: false
        };
    }

    sidebarMinimize() {
        document.body.classList.toggle('sidebar-minimized');
    }

    brandMinimize() {
        document.body.classList.toggle('brand-minimized');
    }

    toggle() {
        this.sidebarMinimize();
        this.brandMinimize();
        this.setState({
            minimized: !this.state.minimized
        });
    }

    handleClick(e) {
        e.preventDefault();
        // Disabled so that Category names are not collapse when clicked
        // e.target.parentElement.classList.toggle('open');
    }

    activeRoute(routeName, props) {
        return props.location.pathname.indexOf(routeName) > -1 ? 'nav-item nav-dropdown open' : 'nav-item nav-dropdown open';
    }

    render() {
        const props = this.props;
        const activeRoute = this.activeRoute;
        const handleClick = this.handleClick;

        const isMinimized = () => {
            return document.body.classList.contains('sidebar-minimized') || document.body.classList.contains('brand-minimized');
        };

        // badge addon to NavItem
        const badge = (badge) => {
            if (badge) {
                const classes = classNames('badge-pill', badge.class);
                return (<Badge id={ this.state.minimized ? 'MinimizedBadgePill' : '' } className={ classes } color={ badge.variant }>{ badge.text }</Badge>);
            }
        };

        const updateBadgeItemCount = (item) => {
            if (item.badge.match) {
                const newItemsCount = codeableItemsCount[item.badge.match.section][item.badge.match.title];

                if (newItemsCount > 0) {
                    item.badge.text = newItemsCount;
                }

            }
        };

        // simple wrapper for nav-title item
        const wrapper = item => {
            return (item.wrapper && item.wrapper.element ? (React.createElement(item.wrapper.element, item.wrapper.attributes, item.name)) : item.name );
        };

        // nav list section title
        const title = (title, key) => {
            const classes = classNames('nav-title', title.class);
            return (<li key={ key } className={ classes }>{ wrapper(title) } </li>);
        };

        // nav list divider
        const divider = (divider, key) => (<li key={ key } className="divider"></li>);

        // nav item with nav link
        const navItem = (item, key) => {
            const classes = classNames(item.class);
            const variant = classNames('nav-link', item.variant ? `nav-link-${item.variant}` : '');
            updateBadgeItemCount(item);

            return (
                <NavItem key={ key } className={ classes }>
                    { isExternal(item.url) ?
                        <RsNavLink href={ item.url } className={ variant } activeClassName="active">
                            <i className={ item.icon }></i>
                            { item.name }
                            { item.badge.text ? badge(item.badge) : null }
                        </RsNavLink>
                        :
                        <NavLink to={ item.url } className={ variant } activeClassName="active">
                            <i className={ item.icon }></i>
                            { item.name }
                            { item.badge.text ? badge(item.badge) : null }
                        </NavLink>
                    }
                </NavItem>
            );
        };

        // nav dropdown
        const navDropdown = (item, key) => {
            updateBadgeItemCount(item);

            console.log('this should work');

            return (
                <li key={ key } className={ activeRoute(item.url, props) }>
                    { this.state.minimized ? badge(item.badge) : null }
                    <a className="nav-link nav-dropdown-toggle" href='#' onClick={ handleClick.bind(this) }>
                        <i className={ item.icon }></i>
                        { item.name }
                    </a>
                    <ul id={ this.state.minimized ? 'MinimizedNavItems' : '' } className="nav-dropdown-items">
                        { navList(item.children) }
                    </ul>
                </li>);
        };

        // nav link
        const navLink = (item, idx) =>
            item.title ? title(item, idx) :
                item.divider ? divider(item, idx) :
                    item.children ? navDropdown(item, idx)
                        : navItem(item, idx);

        // nav list
        const navList = (items) => {
            return items.map((item, index) => navLink(item, index));
        };

        // sidebar-nav root
        return (
            <div className="sidebar">
                <SidebarHeader/>
                <SidebarForm/>
                <nav className="sidebar-nav">
                    <Nav>
                        { navList(nav.items) }
                    </Nav>
                </nav>
                <SidebarFooter/>
                <button
                    className="sidebar-minimizer"
                    type="button"
                    onClick={ (event) => { this.toggle() } }
                >
                </button>
            </div>
        );
    }
}

export default Sidebar;
