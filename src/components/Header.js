/**
 * Beanworks 2017
 *
 * @flow
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { Link } from "react-router-dom";
import { logoutRequest } from '../redux/modules/Auth';
import { /*Badge, */ Dropdown, DropdownMenu, DropdownItem, Nav, NavItem, NavLink, NavbarToggler, NavbarBrand, DropdownToggle } from 'reactstrap';

class Header extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            dropdownOpen: false
        };
    }

    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    sidebarToggle(e) {
        e.preventDefault();
        document.body.classList.toggle('sidebar-hidden');
    }

    sidebarMinimize(e) {
        e.preventDefault();
        document.body.classList.toggle('sidebar-minimized');
    }

    mobileSidebarToggle(e) {
        e.preventDefault();
        document.body.classList.toggle('sidebar-mobile-show');
    }

    asideToggle(e) {
        e.preventDefault();
        document.body.classList.toggle('aside-menu-hidden');
    }

    render() {
        return (
            <header className="app-header navbar">
                <NavbarToggler className="d-lg-none" onClick={ this.mobileSidebarToggle }>&#9776;</NavbarToggler>
                <NavbarBrand href="#">
                </NavbarBrand>
                <NavbarToggler className="d-md-down-none mr-auto" onClick={ this.sidebarToggle }>&#9776;</NavbarToggler>
                <Nav className="ml-auto" navbar>
                    <NavItem className="d-md-down-none">
                        <NavLink href="#">
                            <i className="icon-heart"> Refer Beanworks</i>
                        </NavLink>
                    </NavItem>
                    <NavItem className="d-md-down-none">
                        <NavLink href="#">
                            <i className="icon-present"> </i>
                        </NavLink>
                    </NavItem>
                    <NavItem className="d-md-down-none">
                        <NavLink href="#">
                            <i className="icon-bell"> </i>
                            { /* To display number of items, use the line below: */ }
                            { /*<Badge pill color="danger">5</Badge>*/ }
                        </NavLink>
                    </NavItem>
                    <NavItem className="d-md-down-none">
                        <NavLink href="#">
                            <i className="icon-speech"> </i>
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <Dropdown isOpen={ this.state.dropdownOpen } toggle={ this.toggle }>
                            <DropdownToggle className="nav-link dropdown-toggle">
                                { /* For picture use className="img-avatar" */ }
                                <span className="d-md-down-none">admin</span>
                            </DropdownToggle>
                            <DropdownMenu right className={ this.state.dropdownOpen ? 'show' : '' }>
                                <DropdownItem>
                                    <i className="fa fa-user"> </i> Profile
                                </DropdownItem>
                                <DropdownItem>
                                    <i className="fa fa-cog"> </i> Settings
                                </DropdownItem>
                                <DropdownItem>
                                    <i className="fa fa-question"> </i> Help
                                </DropdownItem>
                                <DropdownItem>
                                    <i className="fa fa-sign-out"> </i>
                                    <a onClick={ this.props.logout }> Logout</a>
                                    {/*<Link to="/logout"> Logout</Link>*/}
                                </DropdownItem>
                            </DropdownMenu>
                        </Dropdown>
                    </NavItem>
                </Nav>
                <NavbarToggler className="d-md-down-none" type="button" onClick={ this.asideToggle }>
                    &#9776;
                </NavbarToggler>
            </header>
        )
    }
}

export default connect(
    state => ({
        state
        // email: state.getIn(['auth', 'email']),
    }),
    dispatch => {
        return {
            logout: () => dispatch(logoutRequest()),
            // navigate: bindActionCreators(NavigationActions.navigate, dispatch),
            // setLoginEmail: bindActionCreators(setLoginEmail, dispatch),
            // setLoginPassword: bindActionCreators(setLoginPassword, dispatch),
            // initiateLogin: bindActionCreators(initiateLogin, dispatch),
            // autoLogin: bindActionCreators(autoLogin, dispatch),
        };
    }
)(Header);
