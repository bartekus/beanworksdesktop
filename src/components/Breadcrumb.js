/**
 * Beanworks 2017
 *
 * @flow
 */

import React from 'react';
import {Route, Link} from 'react-router-dom';
import {Breadcrumb, BreadcrumbItem} from 'reactstrap';
import routes from '../components/SidebarNav';

const findRouteName = url => {
    let routeName = null;

    routes.items.map(route => {
        if (route.children) {
            route.children.map(subRoute => {
                return (url === subRoute.url) ? routeName = subRoute.name : null;
            })
        }

        return (url === route.url) ? routeName = route.name : null;
    });

    return routeName;
};

const getPaths = (pathname) => {
    const paths = ['/'];
    if (pathname === '/') { return paths; }

    pathname.split('/').reduce((prev, curr, index) => {
        const currPath = `${prev}/${curr}`;
        paths.push(currPath);
        return currPath;
    });

    return paths;
};

const BreadcrumbsItem = ({...rest, match}) => {
    const routeName = findRouteName(match.url);

    if (routeName) {
        return (
            match.isExact ?
                (
                    <BreadcrumbItem active>{routeName}</BreadcrumbItem>
                ) :
                (
                    <BreadcrumbItem>
                        <Link to={match.url || ''}>
                            {routeName}
                        </Link>
                    </BreadcrumbItem>
                )
        );
    }
    return null;
};

const Breadcrumbs = ({...rest, location : {pathname}, match}) => {
    const paths = getPaths(pathname);
    const items = paths.map((path, i) => <Route key={ i++ } path={ path } component={ BreadcrumbsItem }/>);
    return (
        <Breadcrumb>
            { items }
        </Breadcrumb>
    );
};

export default props => (
    <div>
        <Route path="/:path" component={Breadcrumbs} {...props} />
    </div>
);
