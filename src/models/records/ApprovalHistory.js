/**
 * Beanworks 2017
 *
 * @flow
 */

import { Record } from 'immutable';
import User from './User';
import Comment from './Comment';

const ApprovalHistoryRecord = new Record(
    {
        id: null,
        action: null,
        created: null,
        explicit: null,
        valid: null,
        user: new User(),
        comment: new Comment(),
    },
    'ApprovalHistory',
);

export type ApprovalHistoryResponse = {
    action: Object,
    user: Object,
    comment: Object,
    created: string,
};

class ApprovalHistory extends ApprovalHistoryRecord {
    static fromJS = (json: ApprovalHistoryResponse = {}): ApprovalHistory => {
        let approvalHistory = new ApprovalHistory(json);
        return approvalHistory
            .set('user', User.fromJS(json.user))
            .set('comment', Comment.fromJS(json.comment));
    };

    static ACTION_APPROVED = 'Approved';
    static ACTION_OVERRIDE_MESSAGE = 'OverrideMessage';
    static ACTION_REJECTED = 'Rejected';
    static ACTION_RESET = 'Reset';
}

export default ApprovalHistory;
