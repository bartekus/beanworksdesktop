/**
 * Beanworks 2017
 *
 * @flow
 */

// Tested through invoice
import { List, Record } from 'immutable';
import BWListItemCollection from '../collections/BWListItemCollection';
import MonetaryItem from './MonetaryItem';
import MonetaryItemTaxCollection from '../collections/MonetaryItemTaxCollection';

const LineItemRecord = new Record(
    Object.assign(MonetaryItem, {
        tip: 0, // Only for Receipt
        lists: new BWListItemCollection(),
        taxes: new List(),
        orgunit: null, // Has to be this for Receipt Line Item to work
        sequence: undefined,
        unitcost: undefined,
        quantity: undefined,
        received: undefined,
    }),
    'LineItem',
);

type LineItemResponse = {
    lists: Object,
    taxes: Array<Object>,
};

export default class LineItem extends LineItemRecord {
    static fromJS(json: LineItemResponse): LineItem {
        return new LineItem(json).set('lists', BWListItemCollection.fromJS(json.lists)).set('taxes', MonetaryItemTaxCollection.fromJS(json.taxes));
    }

    static CODING_TYPE_LINE_ITEM = 'lineItems';
    static CODING_TYPE_RECEIPT_LINE_ITEM = 'receiptLineItem';
}
