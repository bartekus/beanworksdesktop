/**
 * Beanworks 2017
 */

// @flow

import { Record } from 'immutable';
import Comment from '../Comment';
import Fixture from './__fixtures__/Comment.json';

describe('Comment Record', () => {
    it('Should set the correct default', () => {
        const comment = new Comment();
        expect(comment instanceof Comment).toEqual(true);
        expect(comment.toJS()).toMatchSnapshot();
    });

    describe('When loaded with actual data', () => {
        let comment;
        beforeEach(() => {
            comment = new Comment.fromJS(JSON.parse(JSON.stringify(Fixture)));
        });

        it('Should match snapshot', () => {
            expect(comment).toMatchSnapshot();
            expect(comment.get('user')).toMatchSnapshot();
            expect(comment.get('user') instanceof Record).toEqual(true);
        });
    });
});
