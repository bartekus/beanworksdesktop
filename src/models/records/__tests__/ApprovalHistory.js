/**
 * Beanworks 2017
 *
 * @flow
 */

import ApprovalHistory from '../ApprovalHistory';
import User from '../User';
import Comment from '../Comment';
import Fixture from './__fixtures__/ApprovalHistory.json';

describe('ApprovalHistory Record', () => {
    it('Should set the correct default', () => {
        const approvalHistory = new ApprovalHistory();
        expect(approvalHistory instanceof ApprovalHistory).toEqual(true);
        expect(approvalHistory.toJS()).toMatchSnapshot();
    });

    describe('When loaded with actual data', () => {
        let approvalHistory;
        beforeEach(() => {
            approvalHistory = new ApprovalHistory.fromJS(JSON.parse(JSON.stringify(Fixture)));
        });

        it('Should match snapshot', () => {
            expect(approvalHistory).toMatchSnapshot();
            expect(approvalHistory.get('comment')).toMatchSnapshot();
            expect(approvalHistory.get('comment') instanceof Comment).toEqual(true);
            expect(approvalHistory.get('user')).toMatchSnapshot();
            expect(approvalHistory.get('user') instanceof User).toEqual(true);
        });
    });
});
