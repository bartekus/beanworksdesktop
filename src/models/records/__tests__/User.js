/**
 * Beanworks 2017
 *
 * @flow
 */

import User from '../User';
import Fixture from './__fixtures__/User.json';

describe('User Record', () => {
    it('Should set the correct default', () => {
        const user = new User();
        expect(user instanceof User).toEqual(true);
        expect(user.toJS()).toMatchSnapshot();
    });

    describe('When loaded with actual data', () => {
        let user;
        beforeEach(() => {
            user = new User.fromJS(JSON.parse(JSON.stringify(Fixture)));
        });

        it('Should match snapshot', () => {
            expect(user).toMatchSnapshot();
        });
    });
});
