/**
 * Beanworks 2017
 *
 * @flow
 */

import OrgUnit from '../OrgUnit';
import Invoice from '../Invoice';
import fixture from './__fixtures__/OrgUnit.json';

describe('OrgUnit Record', () => {
    const orgunit = OrgUnit.fromJS(JSON.parse(JSON.stringify(fixture)));
    describe('When instantiated', () => {
        it('Should match to snapshot', () => {
            expect(orgunit).toMatchSnapshot();
        });
    });

    describe('When getCodingByType is invoked', () => {
        it('Should exception when no argument is passed in', () => {
            expect(() => {
                orgunit.getCodingByType();
            }).toThrowError();
        });

        it('Should return the corresponding coding', () => {
            expect(orgunit.getCodingByType(Invoice.CODING_TYPE)).toMatchSnapshot();
        });
    });
});