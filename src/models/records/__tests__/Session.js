/**
 * Beanworks 2017
 *
 * @flow
 */

import Session from '../Session';
import { Record, List, OrderedMap } from 'immutable';
import OrgUnit from '../OrgUnit';

import multiLegalEntityFixture from './__fixtures__/Session_Multi_Legal_Entity.json';
import multiLegalEntityWithUserNotInRootOrganizationUnitFixture from './__fixtures__/Session_Multi_Legal_Entity_With_User_Not_In_Root_Organization_Unit';
import singleLegalEntityWithUserInRootOrganizationUnitFixture from './__fixtures__/Session_Single_Legal_Entity_With_User_In_Root_Organization_Unit';

describe('When it is instantiated', () => {
    it('Should set the correct default', () => {
        const session = new Session();
        expect(session instanceof Record).toBeTruthy();
        expect(session.toJS()).toMatchSnapshot();
    });

    describe('When Loaded with actual data for multi entity with a user from root organization unit', () => {
        let session;
        beforeEach(() => {
            session = Session.fromJS(JSON.parse(JSON.stringify(multiLegalEntityFixture)));
        });

        it('Should should match to snapshot', () => {
            expect(session.toJS()).toMatchSnapshot();
        });

        it('Should contain collection of the accessibleOrgUnits type', () => {
            expect(session.accessibleOrgUnits instanceof List).toEqual(true);
        });

        it('Should return proper children count when getMultiEntityChildren is invoked', () => {
            expect(session.getMultiEntityChildren()).toMatchSnapshot();
            expect(session.getMultiEntityChildren().count()).toEqual(multiLegalEntityFixture.fullOrgUnitStructure.children.length);
        });

        it('Should return a value when getRootouId is invoked', () => {
            expect(session.getRootouId()).toBeTruthy();
        });

        it('Should return false when isSingleEntity is invoked', () => {
            expect(session.isSingleEntity()).toBeFalsy();
        });

        it('Should return true when isInRootOu is invoked', () => {
            expect(session.isInRootOu()).toEqual(true);
        });

        it('Should return appropriate count when getAllLegalEntities is invoked', () => {
            expect(session.getAllLegalEntities()).toMatchSnapshot();
            expect(session.getAllLegalEntities().count()).toEqual(6);
        });

        it('Should return appropriate count when getAccessibleOrgUnits is invoked', () => {
            expect(session.getAccessibleOrgUnits()).toMatchSnapshot();
            expect(session.getAccessibleOrgUnits().count()).toEqual(1);
        });

        it('Should return appropriate count when getAllMultiLegalEntities is invoked', () => {
            expect(session.getAllMultiLegalEntities()).toMatchSnapshot();
            expect(session.getAllMultiLegalEntities().count()).toEqual(6);
        });

        it('Should return appropriate count when getAllMultiLegalEntities is invoked', () => {
            expect(session.getAllMultiLegalEntities()).toMatchSnapshot();
            expect(session.getAllMultiLegalEntities().count()).toEqual(6);
        });

        it('Should return the same results as getAllMultiLegalEntities when getAccessibleLegalEntities is invoked', () => {
            expect(session.getAccessibleLegalEntities()).toEqual(session.getAllMultiLegalEntities());
        });

        it('Should return all the legal entities when getAccessibleLegalEntities is invoked', () => {
            const accessibleLegalEntities = session.getAccessibleLegalEntities();
            expect(accessibleLegalEntities instanceof OrderedMap).toBeTruthy();
            expect(accessibleLegalEntities.count()).toBe(6);
            expect(
                accessibleLegalEntities.map(legalEntity => {
                    return legalEntity.get('display');
                }),
            ).toMatchSnapshot();
        });
    });

    describe('When Loaded with actual data for multi entity with a user not from root organization unit', () => {
        let session;
        beforeEach(() => {
            session = Session.fromJS(JSON.parse(JSON.stringify(multiLegalEntityWithUserNotInRootOrganizationUnitFixture)));
        });

        it('Should should match to snapshot', () => {
            expect(session.toJS()).toMatchSnapshot();
        });

        it('Should contain collection of the accessibleOrgUnits type', () => {
            expect(session.accessibleOrgUnits instanceof List).toEqual(true);
        });

        it('Should return proper children count when getMultiEntityChildren is invoked', () => {
            expect(session.getMultiEntityChildren()).toMatchSnapshot();
            expect(session.getMultiEntityChildren().count()).toEqual(multiLegalEntityWithUserNotInRootOrganizationUnitFixture.fullOrgUnitStructure.children.length);
        });

        it('Should return a value when getRootouId is invoked', () => {
            expect(session.getRootouId()).toBeTruthy();
        });

        it('Should return false when isSingleEntity is invoked', () => {
            expect(session.isSingleEntity()).toBeFalsy();
        });

        it('Should return false when isInRootOu is invoked', () => {
            expect(session.isInRootOu()).toEqual(false);
        });

        it('Should return appropriate count when getAllLegalEntities is invoked', () => {
            expect(session.getAllLegalEntities()).toMatchSnapshot();
            expect(session.getAllLegalEntities().count()).toEqual(8);
        });

        it('Should return appropriate count when getAccessibleOrgUnits is invoked', () => {
            expect(session.getAccessibleOrgUnits()).toMatchSnapshot();
            expect(session.getAccessibleOrgUnits().count()).toEqual(2);
        });

        it('Should return appropriate count when getAllMultiLegalEntities is invoked', () => {
            expect(session.getAllMultiLegalEntities()).toMatchSnapshot();
            expect(session.getAllMultiLegalEntities().count()).toEqual(2);
        });

        it('Should return the same results as getAllMultiLegalEntities when getAccessibleLegalEntities is invoked', () => {
            expect(session.getAccessibleLegalEntities()).toEqual(session.getAllMultiLegalEntities());
        });

        it('Should return all the legal entities when getAccessibleLegalEntities is invoked', () => {
            const accessibleLegalEntities = session.getAccessibleLegalEntities();
            expect(accessibleLegalEntities instanceof OrderedMap).toBeTruthy();
            expect(accessibleLegalEntities.count()).toBe(2);
            expect(
                accessibleLegalEntities.map(legalEntity => {
                    return legalEntity.get('display');
                }),
            ).toMatchSnapshot();
        });
    });

    describe('When Loaded with actual data for single entity with a user from non legal entity organization unit', () => {
        let session;
        beforeEach(() => {
            session = Session.fromJS(JSON.parse(JSON.stringify(singleLegalEntityWithUserInRootOrganizationUnitFixture)));
        });

        it('Should should match to snapshot', () => {
            expect(session.toJS()).toMatchSnapshot();
        });

        it('Should contain collection of the accessibleOrgUnits type', () => {
            expect(session.accessibleOrgUnits instanceof List).toBeTruthy();
        });

        it('Should return proper children count when getMultiEntityChildren is invoked', () => {
            expect(session.getMultiEntityChildren()).toMatchSnapshot();
            expect(session.getMultiEntityChildren().count()).toEqual(singleLegalEntityWithUserInRootOrganizationUnitFixture.fullOrgUnitStructure.children.length);
        });

        it('Should return a value when getRootouId is invoked', () => {
            expect(session.getRootouId()).toBeTruthy();
        });

        it('Should return false when isSingleEntity is invoked', () => {
            expect(session.isSingleEntity()).toBeTruthy();
        });

        it('Should return true when isInRootOu is invoked', () => {
            expect(session.isInRootOu()).toEqual(false);
        });

        it('Should return appropriate count when getAllLegalEntities is invoked', () => {
            expect(session.getAllLegalEntities()).toMatchSnapshot();
            expect(session.getAllLegalEntities().count()).toEqual(6);
        });

        it('Should return appropriate count when getAccessibleOrgUnits is invoked', () => {
            expect(session.getAccessibleOrgUnits()).toMatchSnapshot();
            expect(session.getAccessibleOrgUnits().count()).toEqual(1);
        });

        it('Should return appropriate count when getAllMultiLegalEntities is invoked', () => {
            expect(session.getAllMultiLegalEntities()).toMatchSnapshot();
            expect(session.getAllMultiLegalEntities().count()).toEqual(1);
        });

        it('Should return OrderedMap with RootOUID as key name and value for id property when getAccessibleLegalEntities is invoked', () => {
            expect(session.getAccessibleLegalEntities().get(session.getRootouId()).get('id')).toEqual(session.getRootouId());
        });

        it('Should return all the legal entities when getAccessibleLegalEntities is invoked', () => {
            const accessibleLegalEntities = session.getAccessibleLegalEntities();
            expect(accessibleLegalEntities instanceof OrderedMap).toBeTruthy();
            expect(accessibleLegalEntities.count()).toBe(1);

            const legalEntity = accessibleLegalEntities.first();
            expect(legalEntity instanceof OrgUnit).toBeTruthy();
            expect(legalEntity.get('display')).toBe('Beanworks');
        });
    });
});
