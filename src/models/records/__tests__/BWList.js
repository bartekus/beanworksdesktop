/**
 * Beanworks 2017
 *
 * @flow
 */

import BWList from '../BWList';
import OrgUnit from '../OrgUnit';
import fixture from './__fixtures__/BWList.json';

describe('BWList Record ', () => {
    describe('When parsing', () => {
        it('Should match to snapshot', () => {
            const bwlist = BWList.fromJS(JSON.parse(JSON.stringify(fixture)));
            expect(bwlist.get('orgunit') instanceof OrgUnit).toEqual(true);
            expect(bwlist).toMatchSnapshot();
        });
    });
});
