/**
 * Beanworks 2017
 *
 * @flow
 */

import Image from '../Image';
import { List, Map } from 'immutable';
import fixture from './__fixtures__/Image.json';

describe('Image Record', () => {
    it('Should be able to parse the pages', () => {
        const image = Image.fromJS(JSON.parse(JSON.stringify(fixture)));
        expect(List.isList(image.get('pages'))).toEqual(true);
        expect(Map.isMap(image.get('user'))).toEqual(true);

        expect(image).toMatchSnapshot();
        expect(image.get('pages')).toMatchSnapshot();
        expect(image.get('user')).toMatchSnapshot();
    });
});
