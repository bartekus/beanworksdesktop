/**
 * Beanworks 2017
 *
 * @flow
 */

import { List, Map } from 'immutable';
import BWListItem from '../BWListItem';
import LineItem from '../LineItem';
import MonetaryItemTax from '../MonetaryItemTax';

import LINE_WITH_TAX from './__fixtures__/LineItem_WithTax.json';
import LINE_WITH_OUT_TAX_OR_LIST from './__fixtures__/LineItem_WithoutTaxOrList.json';

describe('LineItem Record', () => {
    describe('When a line item with taxes and lists is parsed', () => {
        let lineItem;
        beforeAll(() => {
            lineItem = LineItem.fromJS(JSON.parse(JSON.stringify(LINE_WITH_TAX)));
        });

        it('Should match snapshot', () => {
            expect(lineItem).toMatchSnapshot();
        });

        it('Should have MonetaryItemTaxCollection as the value for taxes', () => {
            const taxes = lineItem.get('taxes');

            expect(taxes instanceof List).toEqual(true);
            taxes.forEach((tax) => {
                expect(tax instanceof MonetaryItemTax).toEqual(true);
                expect(tax.get('amount')).not.toEqual(0);
            });
        });

        it('Should have BWListItemCollection as the value for lists', () => {
            const lists = lineItem.get('lists');

            expect(lists instanceof Map).toEqual(true);
            lists.entries(([key, value]) => {
                expect(typeof key).toEqual('string');
                expect(value instanceof BWListItem).toEqual(true);
            });
        });
    });

    describe('When a line item with taxes and lists is parsed', () => {
        let lineItem;
        beforeAll(() => {
            lineItem = LineItem.fromJS(JSON.parse(JSON.stringify(LINE_WITH_OUT_TAX_OR_LIST)));
        });

        it('Should match snapshot', () => {
            expect(lineItem).toMatchSnapshot();
        });

        it('Should not have the value for taxes', () => {
            const taxes = lineItem.get('taxes');

            expect(taxes instanceof List).toEqual(true);
            expect(taxes.size).toEqual(0);
        });

        it('Should not have the value for lists', () => {
            const lists = lineItem.get('lists');

            expect(lists instanceof Map).toEqual(true);
            expect(lists.size).toEqual(0);
        });
    });
});
