/**
 * Beanworks 2017
 *
 * @flow
 */

import Invoice from '../Invoice';
import { Map, List, OrderedSet } from 'immutable';
import BWListItem from '../BWListItem';
import LineItemCollection from '../../collections/LineItemCollection';
import LineItem from '../LineItem';
import fixture from './__fixtures__/Invoice.json';

describe('Invoice Record', () => {
    describe('When parsed', () => {
        let invoice;
        beforeAll(() => {
            invoice = Invoice.fromJS(JSON.parse(JSON.stringify(fixture)));
        });

        it('Should parse lists into a immutable map with string key and BWListItem as Value', () => {
            const lists: Map = invoice.get('lists');
            expect(Map.isMap(lists)).toEqual(true);
            for (const listItem of lists.values()) {
                expect(listItem instanceof BWListItem).toEqual(true);
            }

            expect(lists).toMatchSnapshot();
        });

        it('Should parse lineitems into Line Items collection and each value should be line item record', () => {
            const lineItems: LineItemCollection = invoice.get('lineItems');
            expect(OrderedSet.isOrderedSet(lineItems)).toEqual(true);
            lineItems.values(lineItem => {
                expect(lineItem instanceof LineItem).toEqual(true);
            });

            expect(lineItems).toMatchSnapshot();
        });

        it('Should parse presetLineItems into Line Items collection and each value should be line item record', () => {
            const lineItems: LineItemCollection = invoice.get('presetLineItems');
            expect(OrderedSet.isOrderedSet(lineItems)).toEqual(true);
            lineItems.values(lineItem => {
                expect(lineItem instanceof LineItem).toEqual(true);
            });

            expect(lineItems).toMatchSnapshot();
        });
    });
});
