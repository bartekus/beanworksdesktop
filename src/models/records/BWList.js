/**
 * Beanworks 2017
 *
 * @flow
 */

import { Record, Map } from 'immutable';
import OrgUnit from './OrgUnit';

const BWListRecord = new Record(
    {
        id: undefined,
        type: undefined,
        active: undefined,
        created: undefined,
        display: undefined,
        updated: undefined,
        orgunit: new OrgUnit(),
        integrated: undefined,
        association: undefined,
        structTemplate: new Map(),
        displayTemplate: undefined,
    },
    'BWList',
);

export default class BWList extends BWListRecord {
    // These constants are copied from API
    static ASSOCIATION_ABSTRACT_PAYMENT = 'AbstractPayment';
    static ASSOCIATION_CODEABLE_ITEM = 'CodeableItem';
    static ASSOCIATION_IMPORTED_PAYMENT = 'ImportedPayment';
    static ASSOCIATION_INVOICE = 'Invoice';
    static ASSOCIATION_RECEIPT = 'Receipt';
    static ASSOCIATION_LINEITEM = 'LineItem';
    static ASSOCIATION_RECEIPT_LINEITEM = 'ReceiptLineItem';
    static ASSOCIATION_MONETARY_ITEM = 'MonetaryItem';
    static ASSOCIATION_PAYABLE_PAYMENT = 'PayablePayment';
    static ASSOCIATION_PURCHASE_ORDER = 'PurchaseOrder';
    static ASSOCIATION_RECEIVING = 'Receiving';

    static TYPE_APACCOUNT = 'APAccount';
    static TYPE_BANK_ACCOUNT = 'BankAccount';
    static TYPE_BANK_ACCOUNT_IMPORTEDPAYMENT = 'BankAccountImportedPayment';
    static TYPE_BANK_GL_ACCOUNT = 'BankGLAccount';
    static TYPE_BILLING = 'Billing';
    static TYPE_BILLABLE = 'Billable';
    static TYPE_CATEGORY = 'Category';
    static TYPE_CLASS = 'Class';
    static TYPE_CURRENCY = 'Currency';
    static TYPE_CUSTOMER = 'Customer';
    static TYPE_CUSTOMER_JOB = 'CustomerJob';
    static TYPE_DEPARTMENT = 'Department';
    static TYPE_DIVISION = 'Division';
    static TYPE_EMPLOYEE = 'Employee';
    static TYPE_EXCHANGE_RATE_SCHEDULE = 'ExchangeRateSchedule';
    static TYPE_PAYMENT_METHOD = 'BeanPaymentMethod';
    static TYPE_PAYMENT_TERMS = 'PaymentTerm';
    static TYPE_FREE_ON_BOARD = 'FreeOnBoard';
    static TYPE_FUND_SOURCE = 'FundSource';
    static TYPE_GENERALLEDGER = 'GeneralLedger';
    static TYPE_GENERALLEDGER_GROUP = 'GeneralLedgerGroup';
    static TYPE_GROUP = 'Group';
    static TYPE_HOLD_PAYMENT = 'HoldPayment';
    static TYPE_IMPORTED_PAYMENT = 'ImpPayment';
    static TYPE_INVENTORY_LOCATION = 'InventoryLocation';
    static TYPE_ITEM = 'Item';
    static TYPE_JOB = 'Job';
    static TYPE_JOB_COST_ITEM = 'CostItem';
    static TYPE_JOB_COST_TYPE = 'CostType';
    static TYPE_JOB_SITE = 'JobSite';
    static TYPE_LANDED_COST = 'LandedCost';
    static TYPE_REASON_CODE = 'ReasonCode';
    static TYPE_SALES_TAX_CODE = 'SalesTaxCode';
    static TYPE_SEPARATE_CHEQUE = 'SeparateCheque';
    static TYPE_SHIPPING_METHOD = 'ShippingMethod';
    static TYPE_OTHER = 'SubAccount';
    static TYPE_PREPAID = 'Prepaid';
    static TYPE_PROJECT = 'Project';
    static TYPE_PROJECT_ACTIVITY = 'ProjectActivity';
    static TYPE_PROPERTY = 'Property';
    static TYPE_SHIPPING = 'Shipping';
    static TYPE_TAG = 'Tag';
    static TYPE_TAX = 'Tax';
    static TYPE_TAX_CLASS = 'TaxClass';
    static TYPE_TAX_GROUP = 'TaxGroup';
    static TYPE_UNIT_MEASURE = 'UnitMeasure';
    static TYPE_VENDOR = 'Vendor';
    static TYPE_VENDOR_CLASS = 'VendorClass';

    static fromJS = (json): BWList => {
        json.orgunit = OrgUnit.fromJS(json.orgunit);
        return new BWList(json);
    };
}
