/**
 * Beanworks 2017
 *
 * @flow
 */

import Immutable, { Record } from 'immutable';
import PageCollection from '../collections/PageCollection';
import Session from './Session';

const ImageRecord = new Record(
    {
        id: null,
        user: new Session(), // or {} or new User?
        pages: new PageCollection(),
        viewed: false,
        updated: null,
        created: null,
        ignored: false,
        importedmethod: null,
        processmethod: null,
    },
    'Image',
);

export type ImageResponse = {
    pages: Array<Object>,
    user: Object,
};

// FlowFixMe
class Image extends ImageRecord {
    static fromJS = (json: ImageResponse = {}): Image => {
        return new Image(json).set('pages', PageCollection.fromJS(json.pages)).set('user', Immutable.fromJS(json.user));
    };

    static IMPORT_METHOD_MOBILE = 'Mobile';
    static PROCESS_METHOD_AC = 'AutoCapture';
    static PROCESS_METHOD_MC = 'ManualCapture';
    static PROCESS_METHOD_NONE = 'None'; //for PO's and Invoices Created From Expense
    static PROCESS_METHOD_RECEIPT = 'Receipt';
}

export default Image;
