/**
 * Beanworks 2017
 *
 * @flow
 */

// Tested through image
import { Record } from 'immutable';

const PageRecord: Record = new Record(
    {
        id: null,
        src: null,
        backup: false,
        created: null,
        updated: null,
        filename: null,
        rotation: null,
        generated: false,
        originalfilemd5: null,
    },
    'Page',
);

class Page extends PageRecord {}

export default Page;
