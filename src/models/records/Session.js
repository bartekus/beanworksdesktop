/**
 * Beanworks 2017
 *
 * @flow
 */

import { List, Record, OrderedMap } from 'immutable';
import OrgUnitCollection from '../collections/OrgUnitCollection';
import OrgUnit from './OrgUnit';

// $FlowFixMe
const SessionRecord = new Record(
    {
        id: undefined,
        roles: new List(), // TODO: make this first class object
        active: true,
        display: '',
        orgTree: new List(),
        orgunit: new OrgUnit(),
        settings: new List(), // TODO: make this first class object
        userType: undefined,
        username: '',
        lastname: '',
        firstname: '',
        permissions: new List(),
        rootOrgUnit: new OrgUnit(),
        featureFlags: new List(),
        accessibleOrgUnits: new OrgUnitCollection(),
        fullOrgUnitStructure: new OrgUnit(),
        access_token: undefined, // eslint-disable-line
        refresh_token: undefined, // eslint-disable-line
        expiry: undefined,
        podNumber: undefined,
    },
    'Session',
);

class Session extends SessionRecord {
    static fromJS = (json: Object = {}): Session => {
        json.roles = new List(json.roles);
        json.orgTree = new List(json.orgTree);
        json.settings = new List(json.settings);
        json.permissions = new List(json.permissions);
        json.featureflags = new List(json.featureFlags);

        json.accessibleOrgUnits = OrgUnitCollection.fromJS(json.accessibleOrgUnits);

        json.orgunit = OrgUnit.fromJS(json.orgunit);
        json.rootOrgUnit = OrgUnit.fromJS(json.rootOrgUnit);
        json.fullOrgUnitStructure = OrgUnit.fromJS(json.fullOrgUnitStructure);

        return new Session(json);
    };

    getFullOrgUnitStructure = () => {
        return this.get('fullOrgUnitStructure');
    };

    getMultiEntityChildren = () => {
        return this.getFullOrgUnitStructure().get('children');
    };

    getRootouId = () => {
        return this.getFullOrgUnitStructure().get('id');
    };

    isSingleEntity = (): boolean => {
        return this.getFullOrgUnitStructure().get('legalEntity');
    };

    isInRootOu = (): boolean => {
        return this.getIn(['orgunit', 'id']) === this.getRootouId();
    };

    getAllLegalEntities = () => {
        return this.getMultiEntityChildren().reduce((carry: OrderedMap, orgunit: OrgUnit): Object => {
            return carry.set(orgunit.get('id'), orgunit);
        }, new OrderedMap());
    };

    getAccessibleOrgUnits = () => {
        return this.get('accessibleOrgUnits').unshift(this.get('orgunit'));
    };

    getAllMultiLegalEntities = () => {
        if (this.isInRootOu()) {
            return this.getAllLegalEntities();
        } else {
            const accessibleOrgunitsIds: Array<string> = this.getAccessibleOrgUnits()
                .map((orgunit: OrgUnit): string => {
                    return orgunit.get('id');
                })
                .toArray();


            return this.getAllLegalEntities().filter((legalEntity, id) => {
                return accessibleOrgunitsIds.indexOf(id) !== -1;
            });
        }
    };

    getAccessibleLegalEntities = (): OrderedMap<string, OrgUnit> => {
        if (this.isSingleEntity()) {
            return new OrderedMap({ [this.getRootouId()]: this.getFullOrgUnitStructure() });
        } else {
            return this.getAllMultiLegalEntities();
        }
    };
}

export default Session;
