/**
 * Beanworks 2017
 *
 * @flow
 */

import { Record } from 'immutable';
import User from './User';

const CommentRecord: Record = new Record(
    {
        id: null,
        created: null,
        text: null,
        user: new User(),
    },
    'Comment',
);

export type CommentResponse = {
    id: string,
    user: Object,
    created: string,
    text: string,
};

class Comment extends CommentRecord {
    static fromJS = (json: CommentResponse): Comment => {
        let comment = new Comment(json);
        return comment
            .set('user', User.fromJS(json.user));
    };
}

export default Comment;
