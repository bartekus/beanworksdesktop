/**
 * Beanworks 2017
 *
 * @flow
 */

import { List, Record } from 'immutable';

const OrgUnitRecord = new Record(
    {
        id: null,
        coding: null,
        details: {},
        display: null,
        children: new List(),
        settings: new List(), // TODO: make this first class object
        legalEntity: false,
    },
    'OrgUnit',
);

export type OrgUnitResponse = {
    display: string,
    children: Array<Object>,
    settings: Array<Object>,
};

export type Coding = {
    id: string,
    display: string,
};

// FlowFixMe
class OrgUnit extends OrgUnitRecord {
    static fromJS = (json: OrgUnitResponse = {}) => {
        if (json.children) {
            const children = json.children.map(child => {
                return OrgUnit.fromJS(child);
            });
            json.children = new List(children);
        } else {
            json.children = new List();
        }

        json.settings = new List(json.settings);

        return new OrgUnit(json);
    };

    getCodingByType = (type: string): Array<Coding> => {
        if (!type) {
            throw new TypeError('Expected first argument of getCodingByType to be string, but got ' + typeof type);
        }
        return this.get('coding')[type].filter(coding => coding.active);
    };
}

export default OrgUnit;
