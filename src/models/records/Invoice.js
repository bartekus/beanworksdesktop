/**
 * Beanworks 2017
 *
 * @flow
 */

import Immutable, { List, Record, Map } from 'immutable';
import CodeableItem from './CodeableItem';
import MonetaryItem from './MonetaryItem';
import BWListItemCollection from '../collections/BWListItemCollection';
import LineItemCollection from '../collections/LineItemCollection';
import Image from './Image';

const InvoiceRecord = new Record(
    Object.assign(CodeableItem, MonetaryItem, {
        lists: new BWListItemCollection(),
        dueDate: undefined,
        poNumber: undefined,
        lineItems: new List(),
        presetLineItems: new List(),
        invoiceDate: undefined,
    }),
    'Invoice',
);

export type InvoiceResponse = {
    lists: any,
    owner: {
        display: string,
    },
    lineItems: Array<Object>,
    legalEntity: Object,
    presetLineItems: Array<Object>,
};

export const STATUS_PENDING_APPROVAL = 'PendingApproval';
export const STATUS_APPROVED = 'Approved';
export const STATUS_REJECTED = 'Rejected';
export const STATUS_RESET = 'Reset';

export default class Invoice extends InvoiceRecord {
    static fromJS = (json: InvoiceResponse): Invoice => {
        let invoice = new Invoice(json);
        return invoice
            .set('lists', BWListItemCollection.fromJS(json.lists))
            .set('owner', Immutable.fromJS(json.owner))
            .set('image', Image.fromJS(json.image))
            .set('lineItems', LineItemCollection.fromJS(json.lineItems))
            .set('legalEntity', Immutable.fromJS(json.legalEntity))
            .set('presetLineItems', LineItemCollection.fromJS(json.presetLineItems));
    };

    static CODING_TYPE = 'invoice';
    static STATUS_PENDING_APPROVAL = STATUS_PENDING_APPROVAL;
}
