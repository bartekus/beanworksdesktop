/*
let fixture = {
    'id': 'F39C67BD678640DB878AA642C246B7F0',
    'description': 'Receipt Description',
    'total': 0,
    'created': '2017-08-09T18:13:05+00:00',
    'updated': '2017-08-09T18:13:05+00:00',
    'type': 'Receipt',
    'lists': [],
    'owner': {
        'id': '51D3E0D0A443403C8E854D9D962675A5',
        'display': 'Tom Lei',
        'srcAvatar36px': 'https:\/\/secure.gravatar.com\/avatar\/41352c3707a4661b14884dd9f5c6e618?s=36&d=mm&r=g',
    },
    'legalEntity': { 'id': '1DBB5455AA5F48DF98DEF5156A64201A', 'display': 'Protein Applesauce Inc. (Sage 300)' },
    'display': '',
    'image': {
        'id': '2A6F2B60F8364FA6BCF8A2BE1FBEC63C',
        'user': {
            'id': '51D3E0D0A443403C8E854D9D962675A5',
            'display': 'Tom Lei',
            'srcAvatar36px': 'https:\/\/secure.gravatar.com\/avatar\/41352c3707a4661b14884dd9f5c6e618?s=36&d=mm&r=g',
        },
        'viewed': false,
        'ignored': false,
        'processmethod': 'Receipt',
        'importmethod': 'Upload',
        'created': '2017-07-20T00:11:38+00:00',
        'updated': '2017-07-20T00:11:38+00:00',
        'pages': [{
            'id': '879DB2A43B2546738B941A707A5AD1E4',
            'src': '\/files\/B8D0C2B0E3DBD46BCA80C4F055877CB0?rouid=536E3158E88B43FC962276748C9C980D',
            'rotation': 0,
            'filename': 'mobile-capture-88933c7a-827a-4690-8834-50bd109308c5.jpg',
            'generated': false,
            'originalfilemd5': '4f2af90cb7a2928a112b855c6923479c',
            'created': '2017-07-20T00:11:38+00:00',
            'updated': '2017-07-20T00:11:38+00:00',
            'backup': false,
        }],
    },
    'status': 'Unattached',
    'comments': [{
        'text': 'Hey there',
        'user': {
            'id': '51D3E0D0A443403C8E854D9D962675A5',
            'display': 'Tom Lei',
            'srcAvatar36px': 'https:\/\/secure.gravatar.com\/avatar\/41352c3707a4661b14884dd9f5c6e618?s=36&d=mm&r=g',
        },
        'created': '2017-08-09T18:13:05+00:00',
        'id': '98765471491B4433948339412150B37D',
    }],
    'approvalChannels': [],
    'lineItems': [{
        'id': '5581748CF11B425F92BC07BC9314AD8D',
        'description': '',
        'total': 0,
        'created': '2017-08-09T18:13:05+00:00',
        'updated': '2017-08-09T18:13:05+00:00',
        'type': 'ReceiptLineItem',
        'lists': [],
        'subtotal': 0,
        'sequence': 1,
        'unitcost': 0,
        'quantity': 0,
        'received': 0,
        'orgunit': { 'id': '536E3158E88B43FC962276748C9C980D', 'display': 'Applesauce Ltd.' },
        'taxes': [{
            'id': '6D82F8F3E5E3458EAD09C7366A9D26F6',
            'amount': 0,
            'taxListItem': {
                'id': '93C22726D5FA4E3C9149502949A7EBDB',
                'display': 'Canadian GST collected',
                'structure': { 'id': '2430' },
                'type': 'Tax',
            },
        }],
        'codeableItem': { 'id': 'F39C67BD678640DB878AA642C246B7F0', 'type': 'Receipt' },
        'children': [],
        'tip': '0.00',
    }],
    'presetLineItems': [],
    'receiptDate': '2017-08-08',
    'receiptVendor': 'Earls',
};
*/

/**
 * Beanworks 2017
 *
 * @flow
 */

import Immutable, { List, Record } from 'immutable';
import CodeableItem from './CodeableItem';
import MonetaryItem from './MonetaryItem';
import BWListItemCollection from '../collections/BWListItemCollection';
import LineItemCollection from '../collections/LineItemCollection';
import Image from './Image';

const ReceiptRecord = new Record(
    Object.assign(CodeableItem, MonetaryItem, {
        lists: new BWListItemCollection(),
        lineItems: new List(),
        presetLineItems: new List(),
        receiptDate: null,
        receiptVendor: null,
    }),
    'Receipt',
);

export type ReceiptResponse = {
    lists: any,
    owner: {
        display: string,
    },
    lineItems: Array<Object>,
    legalEntity: Object,
    presetLineItems: Array<Object>,
    receiptDate: string,
    receiptVendor: string,
};

export default class Receipt extends ReceiptRecord {
    static fromJS = (json: ReceiptResponse): Receipt => {
        let receipt = new Receipt(json);
        return receipt
            .set('lists', BWListItemCollection.fromJS(json.lists))
            .set('owner', Immutable.fromJS(json.owner))
            .set('image', Image.fromJS(json.image))
            .set('lineItems', LineItemCollection.fromJS(json.lineItems))
            .set('legalEntity', Immutable.fromJS(json.legalEntity))
            .set('presetLineItems', LineItemCollection.fromJS(json.presetLineItems));
    };

    static CODING_TYPE = 'receipt';
    // static STATUS_PENDING_APPROVAL = 'PendingApproval';
}
