/**
 * Beanworks 2017
 *
 * @flow
 */

import { Record } from 'immutable';

const BWListItemRecord = new Record(
    {
        id: undefined,
        type: undefined,
        active: undefined,
        listId: undefined,
        display: undefined,
        listName: undefined,
        structure: {},
    },
    'BWListItem',
);

export default class BWListItem extends BWListItemRecord {}
