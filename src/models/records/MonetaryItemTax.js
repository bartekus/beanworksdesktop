/**
 * Beanworks 2017
 *
 * @flow
 */

import { Record } from 'immutable';
import BWListItem from './BWListItem';

const MonetaryItemTaxRecord = new Record(
    {
        id: null,
        amount: 0,
        taxListItem: new BWListItem(),
    },
    'MonetaryItemTax',
);

type MonetaryItemTaxResponse = {
    taxListItem: Object,
};

// $FlowFixMe
export default class MonetaryItemTax extends MonetaryItemTaxRecord {
    static fromJS(json: MonetaryItemTaxResponse): MonetaryItemTax {
        return new MonetaryItemTax(json).set('taxListItem', new BWListItem(json.taxListItem));
    }
}
