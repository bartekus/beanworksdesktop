/**
 * Beanworks 2017
 *
 * @flow
 */

import { List, Map } from 'immutable';
import Image from './Image';
import typeof OrgUnitResponse from './OrgUnit';
import typeof ImageResponse from './Image';

export type CodeableItemResponse = {
    id: string,
    date: string,
    type: string,
    image: ImageResponse,
    total: number,
    owner: {
        display: string,
    },
    status: string,
    number: string,
    display: string,
    created: string,
    updated: string,
    subtotal: number,
    comments: Array<Object>,
    externalId: string,
    description: string,
    legalEntity: OrgUnitResponse,
    approvalChannels: Array<Object>,
    approvalHistories: Array<Object>,
};

export default {
    image: new Image(),
    owner: new Map(),
    status: undefined,
    number: undefined,
    display: undefined,
    comments: new List(),
    externalId: undefined,
    legalEntity: new Map(),
    approvalChannels: new List(),
    approvalHistories: new List(),
};
