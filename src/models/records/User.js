/**
 * Beanworks 2017
 *
 * @flow
 */

import { Record } from 'immutable';

const UserRecord: Record = new Record(
    {
        id: null,
        display: null,
        srcAvatar36px: null,
    },
    'User',
);

export type UserResponse = {
    id: string,
    display: string,
    srcAvatar36px: string,
};

export default class User extends UserRecord {
    static fromJS(json: UserResponse): User {
        return new User(json);
    }
}
