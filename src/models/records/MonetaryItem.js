/**
 * Beanworks 2017
 *
 * @flow
 */

export default {
    id: undefined,
    description: undefined,
    total: 0,
    owner: {},
    subtotal: 0,
    created: null,
    updated: null,
    type: undefined,
};
