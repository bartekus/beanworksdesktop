/**
 * Beanworks 2017
 *
 * @flow
 */

import { OrderedMap } from 'immutable';
import Invoice from '../records/Invoice';

export default class InvoiceCollection extends OrderedMap {
    static fromJS(json: Array<Object>): InvoiceCollection {
        const invoices = json.reduce((carry, invoice) => {
            carry[invoice.id] = Invoice.fromJS(invoice);
            return carry;
        }, {});

        return new InvoiceCollection(invoices);
    }
}
