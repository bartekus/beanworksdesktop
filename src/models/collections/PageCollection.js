/**
 * Beanworks 2017
 *
 * @flow
 */

import { List } from 'immutable';
import Page from '../records/Page';

// Tested through image
export default class PageCollection extends List {
    static fromJS = (json: Array = []): PageCollection => {
        const pages = json.map(page => {
            return new Page(page);
        });

        return new PageCollection(pages);
    };
}
