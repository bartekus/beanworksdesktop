/**
 * Beanworks 2017
 *
 * @flow
 */

import { List } from 'immutable';

export default class ImageCollection extends List {}
