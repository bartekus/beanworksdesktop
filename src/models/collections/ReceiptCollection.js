/**
 * Beanworks 2017
 *
 * @flow
 */

import { OrderedMap } from 'immutable';
import Receipt from '../records/Receipt';

export default class ReceiptCollection extends OrderedMap {
    static fromJS(json: Array<Object>): ReceiptCollection {
        const receipts = json.reduce((carry, receipt) => {
            carry[receipt.id] = Receipt.fromJS(receipt);
            return carry;
        }, {});

        return new ReceiptCollection(receipts);
    }
}
