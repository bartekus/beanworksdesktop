/**
 * Beanworks 2017
 *
 * @flow
 */

import { Map } from 'immutable';
import BWListItem from '../records/BWListItem';

export default class BWListItemCollection extends Map {
    // Unlike standard collection, we are indexing the content of the list
    static fromJS(json: Object = {}): BWListItemCollection {
        Object.keys(json).forEach(listId => {
            json[listId] = new BWListItem(json[listId]);
        });

        return new BWListItemCollection(json);
    }
}
