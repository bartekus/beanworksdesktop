/**
 * Beanworks 2017
 *
 * @flow
 */

import LineItemCollection from '../LineItemCollection';

describe('LineItems Collection', () => {
    describe('When fromJS is invoked', () => {
        it('Should return a OrderedSet with sequence as the order', () => {
            const json = [
                { id: 'a', sequence: 99 },
                { id: 'b', sequence: 100 },
                { id: 'c', sequence: 1 },
                { id: 'd', sequence: 0 },
            ];
            expect(LineItemCollection.fromJS(json).toList().map(element => element.get('sequence')).toArray()).toMatchSnapshot();
        });
    });
});