/**
 * Beanworks 2017
 *
 * @flow
 */

import InvoiceCollection from '../InvoiceCollection';
import Invoice from '../../records/Invoice';
import fixture from './__fixtures__/InvoiceCollection.json';

describe('Invoices Collection', () => {
    describe('When fromJS is called', () => {
        let collection;
        beforeEach(() => {
            collection = InvoiceCollection.fromJS(JSON.parse(JSON.stringify(fixture)));
        });

        it('Should parse each element into Invoice Object', () => {
            collection.toList().forEach((invoice: Invoice) => {
                expect(invoice instanceof Invoice).toEqual(true);
            });
        });
    });
});