/**
 * Beanworks 2017
 *
 * @flow
 */

import MonetaryItemTaxCollection from '../MonetaryItemTaxCollection';
import MonetaryItemTax from '../../records/MonetaryItemTax';

describe('MonetaryItemTaxCollection', () => {
    let collection;
    const GST_AMOUNT = 10;
    const PST_AMOUNT = 20;
    let GST;
    let PST;
    beforeEach(() => {
        collection = MonetaryItemTaxCollection.fromJS();
        GST = {
            id: 'gst',
            display: 'GST',
        };
        PST = {
            id: 'pst',
            display: 'PST',
        };
        const gst = MonetaryItemTax.fromJS({
            amount: GST_AMOUNT,
            taxListItem: GST,
        });
        const pst = MonetaryItemTax.fromJS({
            amount: PST_AMOUNT,
            taxListItem: PST,
        });
        collection = collection.push(gst).push(pst);
    });
    describe('When getTaxTotal is invoked', () => {
        it('Should return the total of all the taxes', () => {
            expect(MonetaryItemTaxCollection.getTaxTotal(collection)).toEqual(GST_AMOUNT + PST_AMOUNT);
        });
    });
    // describe('When createModelByTaxListItems is invoked', () => {
    //
    //
    //     it('Should throw TypeError when first argument is not array', () => {
    //         expect(() => {
    //             collection.createModelByTaxListItems('string');
    //         }).toThrow(new TypeError('Expecting array for taxListItems but got string'));
    //     });
    //
    //     it('Should create one MonetaryItemTax for each of the new taxListItem in first argument', () => {
    //         expect(collection.length).toBe(0);
    //         collection.createModelByTaxListItems([GST, PST]);
    //         expect(collection.length).toBe(2);
    //         expect(collection.at(0).get(ModelMonetaryItemTax.ATTR_TAX_LIST_ITEM)).toEqual(GST);
    //         expect(collection.at(0).get(ModelMonetaryItemTax.ATTR_AMOUNT)).toEqual(0);
    //         expect(collection.at(1).get(ModelMonetaryItemTax.ATTR_TAX_LIST_ITEM)).toEqual(PST);
    //         expect(collection.at(1).get(ModelMonetaryItemTax.ATTR_AMOUNT)).toEqual(0);
    //     });
    //
    //     it('Should not do anything if there is already a MonetaryItemTax for the taxListItem', () => {
    //         expect(collection.length).toBe(0);
    //         collection.createModelByTaxListItems([GST, PST]);
    //         expect(collection.length).toBe(2);
    //         collection.createModelByTaxListItems([GST]);
    //         expect(collection.length).toBe(2);
    //     });
    // });
    //
    // describe('When removeModelByTaxListItemsIds is invoked', () => {
    //     beforeEach(() => {
    //         collection.createModelByTaxListItems([GST, PST]);
    //     });
    //     it('Should throw TypeError when first argument is not a Set', () => {
    //         expect(() => {
    //             collection.removeModelByTaxListItemsIds('string');
    //         }).toThrow(new TypeError('Expecting Set for ids but got string'));
    //     });
    //
    //     it('Should delete the model with this taxListItem id', () => {
    //         collection.removeModelByTaxListItemsIds(new Set([GST.id]));
    //         expect(collection.length).toBe(1);
    //         expect(collection.at(0).get(ModelMonetaryItemTax.ATTR_TAX_LIST_ITEM)).toEqual(PST);
    //     });
    //
    //     it('Should do nothing when the supplied ID is not found', () => {
    //         collection.removeModelByTaxListItemsIds(new Set('sdfsdf'));
    //         expect(collection.length).toBe(2);
    //     });
    // });
    //
    // describe('When getModelByTaxListItemId is invoked', () => {
    //     beforeEach(() => {
    //         collection.createModelByTaxListItems([GST, PST]);
    //     });
    //     it('Should return the correct model', () => {
    //         const model = collection.getModelByTaxListItemId(GST.id);
    //         expect(model.get(ModelMonetaryItemTax.ATTR_TAX_LIST_ITEM))
    //     });
    // });
});