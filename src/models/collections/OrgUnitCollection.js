/**
 * Beanworks 2017
 *
 * @flow
 */

import { List } from 'immutable';
import OrgUnit from '../records/OrgUnit';

export default class OrgUnitCollection extends List {
    static fromJS = (json: Array = []): OrgUnitCollection => {
        const orgunits = json.map(orgunit => {
            return new OrgUnit(orgunit);
        });

        return new OrgUnitCollection(orgunits);
    };
}
