/**
 * Beanworks 2017
 *
 * @flow
 */

import { List } from 'immutable';
import MonetaryItemTax from '../records/MonetaryItemTax';

// Tested through LineItem
export default class MonetaryItemTaxCollection extends List {
    static fromJS = (json: Array<Object> = []): MonetaryItemTaxCollection => {
        const monetaryItemTaxCollection = json.map(monetaryItemTax => {
            return MonetaryItemTax.fromJS(monetaryItemTax);
        });

        return new MonetaryItemTaxCollection(monetaryItemTaxCollection);
    };

    static getTaxTotal(collection: MonetaryItemTaxCollection): number {
        return collection.reduce((total, monetaryItemTax: MonetaryItemTax) => {
            return total + monetaryItemTax.get('amount');
        }, 0);
    }
}
