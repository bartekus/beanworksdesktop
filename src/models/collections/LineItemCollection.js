/**
 * Beanworks 2017
 *
 * @flow
 */

import { OrderedSet } from 'immutable';
import LineItem from '../records/LineItem';

// Tested through Invoice
export default class LineItemCollection extends OrderedSet {
    static fromJS = (json: Array<Object> = []): LineItemCollection => {
        const lineItems = json
            .sort((a, b) => {
                return a.sequence < b.sequence ? -1 : 1;
            })
            .map(lineItem => {
                return LineItem.fromJS(lineItem);
            });

        return new LineItemCollection(lineItems);
    };
}
