/**
 * Beanworks 2017
 *
 * @flow
 */

process.env.ELECTRON_START_URL = `https://localhost:3000/`;

const remotedev = require('remotedev-server');
const pem = require('pem');

pem.createCertificate({days:1, selfSigned:true}, function(err, keys){
    remotedev({
        name: 'socketcluster',
        hostname: 'localhost',
        port: 8000,
        realtime: true,
        protocol: 'https',
        passphrase: 'remotedev',
        key: keys.serviceKey,
        cert: keys.certificate,
        suppressConnectErrors: false
    });
});

const net = require('net');
const client = new net.Socket();

let startedElectron = false;

const tryConnection = () => client.connect({ port: 3000 }, () => {
        client.end();
        if(!startedElectron) {
            console.log('starting electron');
            startedElectron = true;
            const exec = require('child_process').exec;
            exec('npm run electron');
        }
    }
);

setTimeout(function(){ tryConnection(); }, 15000);

client.on('error', (error) => { setTimeout(tryConnection, 1000); });
