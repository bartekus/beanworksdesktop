/**
 * Beanworks 2017
 *
 * @flow
 */

import { all, fork } from 'redux-saga/effects'
import { loginSaga, logoutSaga } from './modules/AuthSaga'

// The root saga is what we actually send to Redux's middleware.
// In here we fork each saga so that they are all "active" and listening.
// Sagas are fired once at the start of an app and can be thought of as processes running in the background,
// watching actions dispatched to the store.
const rootFn = function* root() {
    yield all([
        fork(loginSaga),
        fork(logoutSaga),
    ]);
};

export default rootFn;
