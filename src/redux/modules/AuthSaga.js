/**
 * Beanworks 2017
 *
 * @flow
 */

// This file contains the sagas used for async actions in our app.
// It's divided into "effects" that the sagas call (`authorize` and `logout`) and the actual sagas themselves,
// which listen for actions.
// Sagas help us gather all our side effects (network requests in this case) in one place

import { hashSync } from 'bcryptjs'
import genSalt from '../../utils/SaltUtil'
import { take, fork, cancel, call, put, race } from 'redux-saga/effects';
import AuthService from '../../services/AuthService';
import { push } from 'react-router-redux';
import { setAuthentication, setInProgress, loginRequest, logoutRequest, requestError, LOGIN_REQUEST, LOGOUT_REQUEST } from './Auth';
import { setClient, unsetClient } from './Client';

/* Effect to handle logging in */
export function * login ({ username, password }) {
    yield put(setInProgress(true)); // We send an action that tells Redux we're sending a request
    try { // We then try to register or log in the user, depending on the request
        // It's always best to hash any sensitive data before sending it over any networks
        const salt = genSalt(username);
        const hash = hashSync(password, salt);
        console.log(`You are sending readable password so hash it: ${ hash }`);
        let response;
        // We call the asynchronous login function in the `AuthService` module.
        // !!! Since that's not currently supported, we send password as ia :( !!!
        response = yield call(AuthService.login, { username, password }); // Because we're using generators,
        // response = yield call(AuthService.login, username, hash); // Because we're using generators,
        // we can work as if it's synchronous because we pause execution until the call is done with `yield`!
        return response
    } catch (error) {
        const newError = { error: `${ error.response.status }: ${ error.response.statusText }`, message: `${ error.response.data.errorMessages[0] }` };
        console.log(newError);
        yield put(requestError(newError));  // If we get an error we send Redux action and return
        // let parsedError = error;
        // console.log('Raw error', parsedError);
        return false
    } finally {
        // When done, we tell Redux we're not in the middle of a request any more
        yield put(setInProgress(false))
    }
}

/* Effect to handle logging out */
export function* logout() {
    yield put(setInProgress(true)); // We tell Redux we're in the middle of a request

    // Similar to above, we try to log out by calling the `logout` function in the `AuthService` module.
    // If we get an error, we send an appropriate action.
    // If we don't, we return the response.
    try {
        const response = yield call(AuthService.logout);
        yield put(setInProgress(false));

        return response
    } catch (error) {
        yield put(requestError(error))
    }
}

/* Login saga */
export function * loginSaga () {
    // Because sagas are generators, doing `while (true)` doesn't block our program.
    // Basically here we say "this saga is always listening for actions"
    while (true) {
        // And we're listening for `LOGIN_REQUEST` actions and destructuring its payload
        const request = yield take(LOGIN_REQUEST);
        const { username, password } = request.data;

        // A `LOGOUT` action may happen while the `authorize` effect is going on, which may
        // lead to a race condition. This is unlikely, but just in case, we call `race` which
        // returns the "winner", i.e. the one that finished first
        const winner = yield race({
            authorized: call(login, { username, password }),
            logout: take(LOGOUT_REQUEST)
        });

        // If `authorize` was the winner we send the `setAuthentication` Redux action
        if (winner.authorized) {
            yield put(setAuthentication({ expiry: winner.authorized.data.expiry }));
            yield put(setClient(winner.authorized.data));
            yield call(AuthService.instantiateAxios, winner.authorized.data);
            // yield put({type: CHANGE_FORM, newFormState: {username: '', password: ''}}); // Clear form
            yield put(push('/beanboard')); // Go to beanboard page
        }
    }
}

/* Logout saga */
export function * logoutSaga () { // This is basically the same as the `if (winner.logout)` of above,
    while (true) { // just written as a saga that is always listening to `LOGOUT` actions
        yield take(LOGOUT_REQUEST);
        yield put(setAuthentication(false));
        yield put(unsetClient());
        yield call(logout);
        yield put(push('/'));
    }
}
