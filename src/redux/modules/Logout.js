/**
 * Beanworks 2017
 *
 * @flow
 */

import { Map } from 'immutable';

export const CLIENT_LOGOUT = 'CLIENT_LOGOUT';

const initialState = Map({
    logout: false,
});

export const logoutRequest = function logoutRequest() {
    return {
        type: CLIENT_LOGOUT,
    }
};

const reducer = function clientReducer(state = initialState, action) {
    switch (action.type) {
        case CLIENT_LOGOUT:
            return state
                .set('logout', true);

        default:
            return state;
    }
};

export default reducer;
