/**
 * Beanworks 2017
 *
 * @flow
 */

import { Map, fromJS } from 'immutable';
export const SET_IN_PROGRESS = 'auth/SET_IN_PROGRESS';
export const LOGIN_REQUEST = 'auth/LOGIN_REQUEST';
export const SET_AUTHENTICATION = 'auth/SET_AUTHENTICATION';
export const LOGOUT_REQUEST = 'auth/LOGOUT_REQUEST';
export const REQUEST_ERROR = 'auth/REQUEST_ERROR';

const initialState = Map({
    error: null,
    message: null,
    isValid: false,
    expiresAt: null,
    inProgress: false,
    lastCallAt: null,
    redirectTo: '',
});

export function setAuthentication(data) {
    return { type: SET_AUTHENTICATION, data }
}

export function setInProgress(newState) {
    return { type: SET_IN_PROGRESS, newState }
}

export function loginRequest(data) {
    return { type: LOGIN_REQUEST, data }
}

export function logoutRequest() {
    return { type: LOGOUT_REQUEST }
}

export function requestError(data) {
    return { type: REQUEST_ERROR, data }
}

function reducer (state = initialState, action) {
    switch (action.type) {
        case SET_AUTHENTICATION:
            return state
                .set('isValid', true)
                .set('error', null)
                .set('message', null)
                .set('expiresAt', action.data.expiry)
                .set('lastCallAt', new Date())
                .set('redirectTo', '/beanboard');

        case SET_IN_PROGRESS:
            return state
                .set('inProgress', action.newState);

        case REQUEST_ERROR:
            console.log('action.data.error', action.data);
            return state
                .set('isValid', false)
                .set('error', action.data.error)
                .set('message', action.data.message)
                .set('expiresAt', null)
                .set('lastCallAt', new Date())
                .set('redirectTo', '');

        default:
            return state
    }
}

export default reducer;
