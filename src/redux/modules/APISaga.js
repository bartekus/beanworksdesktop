/**
 * Beanworks 2017
 *
 * @flow
 */

import { takeLatest, put, call } from 'redux-saga/effects';

import fetchFailed from 'actions/fetch-failed';
import setRecords from 'actions/set-records';

export default function* onFetchRecords() {
    yield takeLatest('RECORDS/FETCH', function* fetchRecords() {

        try {
            const response = yield fetch('https://api.service.com/endpoint');
            const responseBody = response.json();
        } catch (e) {
            yield put(fetchFailed(e));
            return;
        }

        yield put(setRecords(responseBody.records));
    });
}
