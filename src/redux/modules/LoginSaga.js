/**
 * Beanworks 2017
 *
 * @flow
 */

import { take, fork, cancel, call, put /*, cancelled*/ } from 'redux-saga/effects';
import { handleApiErrors } from '../../utils/ApiErrors';
import { LOGIN_REQUESTING, LOGIN_SUCCESS, LOGIN_ERROR } from './Login';
import { setClient, unsetClient } from './Client';

const loginUrl = `${process.env.REACT_APP_API_URL}/auth/login`;

function loginApi(email, password) {
    const data = new FormData();
    data.append('username', email);
    data.append('password', password);

    return fetch(loginUrl, {
        method: 'POST',
        body: data,
    })
    .then(handleApiErrors)
    .then(response => response.json())
    .then(function (res) {
        if (res.status === "error") {
            var error = new Error(res.message);
            error.response = res.message;
            throw error
        }
        return res
    })
    .catch((error) => {
        throw error
    })
}

function* logout() {
    yield put(unsetClient());
}

function* loginFlow(username, password) {
    let clientData;
    try {
        clientData = yield call(loginApi, username, password);
        if (clientData.errorMessages) {
            const err = clientData.errorMessages;
            yield put({ type: LOGIN_ERROR, err })
        } else {
            yield put(setClient(clientData));
            yield put({ type: LOGIN_SUCCESS });

        }
    } catch (error) {
        yield put({ type: LOGIN_ERROR, error })
    } finally {
        // No matter what, if our `forked` `task` was cancelled we will then just redirect them to login
        //yield put({ type: LOGIN_ERROR, error })
    }
    return clientData
}

function* loginWatcher() {
    while (true) {
        const { username, password } = yield take(LOGIN_REQUESTING);
        const task = yield fork(loginFlow, username, password);
        const action = yield take([LOGIN_ERROR]);

        if (action.type === LOGIN_REQUESTING) {
            yield cancel(task)
        }

        yield call(logout)
    }
}

export default loginWatcher
