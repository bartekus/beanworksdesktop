/**
 * Beanworks 2017
 *
 * @flow
 */

import { Map, fromJS } from 'immutable';
export const LOGIN_REQUESTING = 'LOGIN_REQUESTING';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_ERROR = 'LOGIN_ERROR';

const initialState = Map({
    loading: false,
    data: null,
});

export function loginRequest({ username, password }) {
    return {
        type: LOGIN_REQUESTING,
        username,
        password,
    }
}


const reducer = function (state = {data: [], loading: false}, action = {}) {
    switch (action.type) {
        case 'RECORDS/FETCH':
        case 'RECORDS/FETCH_FAILED':
            return {
                ...state,
                loading: true,
                data: []
            };
        case 'RECORDS/SET':
            return {
                ...state,
                loading: false,
                data: action.payload
            };
        default:
            return state;
    }
};

export default
