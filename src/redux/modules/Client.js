/**
 * Beanworks 2017
 *
 * @flow
 */

import { Map, fromJS } from 'immutable';
import Session from '../../models/records/Session';
import { REHYDRATE } from 'redux-persist-immutable/constants';

export const CLIENT_SET = 'CLIENT_SET';
export const CLIENT_UNSET = 'CLIENT_UNSET';

const initialState = new Session();

export function setClient(clientData) {
    return {
        type: CLIENT_SET,
        clientData,
    }
}

export function unsetClient() {
    return {
        type: CLIENT_UNSET,
    }
}

const reducer = function clientReducer(state = initialState, action) {
    switch (action.type) {
        case CLIENT_SET:
            return fromJS(action.clientData);

        case CLIENT_UNSET:
            return initialState;

        case REHYDRATE:
            let incoming = action.payload.client;

            if (incoming) {
                const fromInc = incoming.toJS();
                const fromStore = state.toJS();
                return fromJS({ ...fromStore, ...fromInc })
            }
            return state;

        default:
            return state
    }
};

export default reducer;
