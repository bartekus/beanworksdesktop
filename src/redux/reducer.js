/**
 * Beanworks 2017
 *
 * @flow
 */

import { combineReducers } from 'redux-immutable';
import { routerReducer } from 'react-router-redux'
import { reducer as formReducer } from 'redux-form/immutable';
import { reducer as intl } from './Intl';
import client from './modules/Client'
import authentication from './modules/Auth';

const reducer = combineReducers({
    authentication,
    client,
    intl,
    form: formReducer,
    router: routerReducer,
});

export default reducer;
