/**
 * Beanworks 2017
 *
 * @flow
 */

import sagaMiddleware from './middleware/SagaMiddleware';
import routerMiddleware from './middleware/RouterMiddleware';
import loggerMiddleware from './middleware/LoggerMiddleware';

export default [
    sagaMiddleware,
    routerMiddleware,
    loggerMiddleware
];
