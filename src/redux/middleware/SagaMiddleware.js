/**
 * Beanworks 2017
 *
 * @flow
 */

import createSagaMiddleware from 'redux-saga';

const sagaMiddleware = createSagaMiddleware();

export default sagaMiddleware;
