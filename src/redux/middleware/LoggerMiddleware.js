/**
 * Beanworks 2017
 *
 * @flow
 */

import { createLogger } from 'redux-logger';

export default createLogger({
    collapsed: true,

    // only log in development mode and ignore `CHANGE_FORM` actions in the logger, since they fire after every keystroke
    predicate: (getState, action) => process.env.NODE_ENV === "development" && action.type !== '@@redux-form/CHANGE',

    // transform immutable state to plain objects
    stateTransformer: state => state.toJS(),

    // transform immutable action payloads to plain objects
    actionTransformer: action =>
        action && action.payload && action.payload.toJS
            ? { ...action, payload: action.payload.toJS() }
            : action
});
