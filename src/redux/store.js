/**
 * Beanworks 2017
 *
 * @flow
 */

import { applyMiddleware, createStore } from 'redux';
import localForage from 'localforage';
import { Map } from 'immutable';
import { persistStore, autoRehydrate } from 'redux-persist-immutable';
import { REHYDRATE } from 'redux-persist/constants';
import createActionBuffer from 'redux-action-buffer';
import immutableTransform from 'redux-persist-transform-immutable';
import { composeWithDevTools } from 'remote-redux-devtools';
import Session from '../models/records/Session';
import OrgUnit from '../models/records/OrgUnit';
import Image from '../models/records/Image';
import middleware from './Middleware';
import sagaMiddleware from './middleware/SagaMiddleware';
import reducer from './reducer';
import saga from './saga';

const persistConfig = {
    storage: localForage,
    whitelist: ['client'],
    blacklist: ['router'],
    // debounce: 500,
    transforms: [
        immutableTransform({
            records: [Session, Image, OrgUnit],
            whitelist: ['client'],
        }),
    ],
};

export default function configureStore() {
    const initialState = Map({});
    const enhancer = composeWithDevTools(applyMiddleware(...middleware, createActionBuffer(REHYDRATE)), autoRehydrate());

    return new Promise((resolve, reject) => {
        try {
            const store = createStore(
                reducer,
                initialState,
                enhancer
            );

            sagaMiddleware.run(saga);

            persistStore(
                store,
                persistConfig,
                () => resolve(store)
            );

            if (module.hot) {
                module.hot.accept(() => {
                    const nextRootReducer = require('./reducer').default;
                    store.replaceReducer(nextRootReducer);
                });
            }
        } catch (e) {
            reject(e);
        }
    });
}
