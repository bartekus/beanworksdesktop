/**
 * Beanworks 2017
 *
 * @flow
 */

import { connect } from 'react-redux';
import { loginRequest } from '../../redux/modules/Auth';
import loginForm from './Login';

export default connect(
    state => ({
        authentication: state.getIn(['authentication']).toJS(),
    }),
    dispatch => ({
        onSubmit: data => dispatch(loginRequest(data.toJS())),
    })
)(loginForm);
