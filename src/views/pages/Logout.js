/**
 * Beanworks 2017
 *
 * @flow
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router';
import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { logoutRequest } from '../../redux/modules/Logout';
import { loginOut } from '../../redux/modules/Login';

class Logout extends Component {
    static propTypes = {
        logout: PropTypes.object,
    };

    componentWillMount() {
        this.props.dispatch(logoutRequest());
        this.props.dispatch(loginOut());
    }

    render() {
        const { logout } = this.props.logout;
        return (
            <div>
                { logout && <Redirect to='/login' push/> }
            </div>)
    }
}

const formed = reduxForm({
    form: 'logout',
})(Logout);

const connected = connect(
    state => ({
        logout: state.getIn(['logout']).toJS(),
    }),
)(formed);

export default connected;
