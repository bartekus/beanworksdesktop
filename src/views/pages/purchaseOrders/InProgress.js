import React, { Component } from 'react';
import { Container } from 'reactstrap';

// loadProducts = () => {
//     return new Promise(resolve => {
//         setTimeout(() => resolve([
//             'Tesla S',
//         ]), 20000);
//     }).then(products => this.setState({ products }));
// };

export default class PurchaseOrdersInProgress extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.props.list == null) {
            this.props.fetch();
        }
    }

    render() {
        const { location, list } = this.props;

        if (list === null) {
            return (<p>{ 'Loading...' }</p>);
        }

        return (
            <Container fluid>
                <h3><code>{location.pathname}</code></h3>
                <ul>
                    {list.map(product => <li key={ product.id }>{product}</li>)}
                </ul>
            </Container>
        )
    }
}
