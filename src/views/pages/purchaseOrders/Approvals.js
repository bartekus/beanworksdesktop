import React, { Component } from 'react';
import { Container } from 'reactstrap';

export default class PurchaseOrdersApprovals extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { location } = this.props;

        return (
            <Container fluid>
                <h3><code>{location.pathname}</code></h3>
            </Container>
        )
    }
}
