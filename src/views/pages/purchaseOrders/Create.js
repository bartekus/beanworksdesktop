import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Container } from 'reactstrap';

export class PurchaseOrdersCreate extends Component {
    constructor(props) {
        super(props);
    }

    // componentWillMount() {
    //     // this.checkAuthentication(this.props);
    // }
    // componentWillReceiveProps(nextProps) {
    //     // if (nextProps.location !== this.props.location) {
    //     //     this.checkAuthentication(nextProps);
    //     // }
    // }

    componentDidMount() {
        if (this.props.list == null) {
            this.props.fetch();
        }
    }

    render() {
        const { location, list } = this.props;

        if (list === null) {
            return (<p>{ 'Loading...' }</p>);
        }

        return (
            <Container fluid>
                <h3><code>{location.pathname}</code></h3>
                <ul>
                    {list.map(product => <li key={ product.id }>{product}</li>)}
                </ul>
            </Container>
        )
    }
}

export default PurchaseOrdersCreate;