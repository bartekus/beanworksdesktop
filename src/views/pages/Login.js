/**
 * Beanworks 2017
 *
 * @flow
 */

import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { reduxForm, Field } from 'redux-form/immutable';
import Message from '../../services/notifications/Messages';
import Error from '../../services/notifications/Errors';
import { Container, Row, Col, CardImg, Button, Input, InputGroup, InputGroupAddon, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import ValidateForm from '../../utils/ValidateForm';
import logo from '../../assets/img/logo.svg';

const renderField = ({ input, label, type, meta: { touched, error }, icon }) => (
    <InputGroup id={ label } className="mb-3">
        <InputGroupAddon id={ `${label}-icon`  }>
            <i className={ icon }> </i>
        </InputGroupAddon>
        <input { ...input } id={ `${label}-input` } type={ type } placeholder={ label } style={ { width: '200px' } } />
        { touched && error && <span>{ error }</span> }
    </InputGroup>
);

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modal: false,
            backdrop: true
        };
        this.toggle = this.toggle.bind(this);
    }

    componentWillReceiveProps(props) {
        console.log('props', props);
        // { isValid && <Redirect to={ redirectTo }/> }
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    render() {
        const {
            handleSubmit,
            pristine,
            submitting,
            authentication: { inProgress, isValid, message, error, redirectTo },
        } = this.props;

        return (
            <div className="app">
                <div className="app flex-row align-items-center">
                    <Container className="animated fadeIn">

                        <Row className="justify-content-center">
                            <Col>{ /* Spacer Column */ }</Col>
                            <Col className="p-4">
                                <CardImg top width="100%" src={ logo } alt="Beanworks"/>
                                {/*{ isValid && <Redirect to={ '/beanboard' }/> }*/}
                                <form className="card-body" onSubmit={ handleSubmit.bind(this) }>
                                    <Field
                                        name="username"
                                        type="text"
                                        component={ renderField }
                                        label="Username"
                                        icon="icon-user"
                                    />
                                    <Field
                                        name="password"
                                        type="password"
                                        component={ renderField }
                                        label="Password"
                                        icon="icon-lock"
                                    />
                                    <Row>
                                        <Button
                                            action="submit"
                                            disabled={ pristine || submitting }
                                            color="primary"
                                            className="px-4"
                                            size="lg"
                                            block
                                            style={ { background: '#0d8a50' } }
                                        >
                                            { inProgress ? 'Logging in...' : 'Sign In' }
                                        </Button>
                                    </Row>
                                </form>
                                <div className="auth-message">
                                    { !inProgress && !!error && (<Error message="Failure to login due to:" error={ error } />) }
                                    { !inProgress && !!message && (<Message message={ message } />) }
                                </div>
                            </Col>
                            <Col>{ /* Spacer Column */ }</Col>
                        </Row>

                        <ul className="support-links">
                            <li>
                                <a href="#ForgotModal" onClick={ this.toggle }>Forgot your password?</a>
                            </li>
                            <li>
                                Need help?
                                <a
                                    href="http://support.beanworks.com"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                > Visit our Support Desk</a>
                            </li>
                            <li>
                                Contact us at
                                <a href="mailto:support@beanworks.com"> support@beanworks.com</a>
                            </li>
                            <li>
                                Copyright
                                <a
                                    href="http://beanworks.com"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                > Beanworks</a>
                            </li>
                        </ul>

                        <Modal
                            isOpen={ this.state.modal }
                            toggle={ this.toggle }
                            className={ this.props.className }
                        >
                            <ModalHeader toggle={ this.toggle }>
                                Reset Password
                            </ModalHeader>
                            <ModalBody>
                                <p>Please enter the email address you use to sign in to Beanworks.</p>
                                <InputGroup>
                                    <Input placeholder="Username" />
                                </InputGroup>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="primary" onClick={ this.toggle }>Send Password Recovery Email</Button>{ ' ' }
                                <Button color="secondary" onClick={ this.toggle }>Cancel</Button>
                            </ModalFooter>
                        </Modal>

                    </Container>
                </div>
            </div>
        );
    }
}

export const loginForm = reduxForm({
    form: 'login',
    ValidateForm,
})(Login);

export default loginForm;
