/**
 * Beanworks 2017
 *
 * @flow
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Switch, Redirect, Route } from 'react-router-dom';

import { Container } from 'reactstrap';
import Header from '../components/Header';
import Sidebar from '../components/Sidebar';
import Breadcrumb from '../components/Breadcrumb';
import Aside from '../components/Aside';
import Footer from '../components/Footer';

// import PreloaderLink from '../utils/PreloaderLink';

import UserPerformance from './pages/UserPerformance';

import PurchaseOrdersCreate from './pages/purchaseOrders/Create';
import PurchaseOrdersInProgress from './pages/purchaseOrders/InProgress';
import PurchaseOrdersApprovals from './pages/purchaseOrders/Approvals';
import PurchaseOrdersCommitted from './pages/purchaseOrders/Committed';
import PurchaseOrdersClosed from './pages/purchaseOrders/InProgress';

import ExpensesCreateReceipts from './pages/expenses/CreateReceipts';
import ExpensesCreateExpenses from './pages/expenses/CreateExpenses';
import ExpensesInProgress from './pages/expenses/InProgress';
import ExpensesApprovals from './pages/expenses/Approvals';
import ExpensesApproved from './pages/expenses/Approved';

import InvoicesApprovals from './pages/invoices/Approvals';
import InvoicesArchived from './pages/invoices/Archived';
import InvoicesCreate from './pages/invoices/Create';
import InvoicesExport from './pages/invoices/Export';
import InvoicesInProgress from './pages/invoices/InProgress';

import PaymentsApprovals from './pages/payments/Approvals';
import PaymentsArchived from './pages/payments/Archived';
import PaymentsCreate from './pages/payments/Create';
import PaymentsInProgress from './pages/payments/InProgress';
import PaymentsPaid from './pages/payments/Paid';
import PaymentsReleases from './pages/payments/Releases';

const NoMatch = ({ location }) => (
    <div>
        <h3>No match for <code>{ location.pathname }</code></h3>
    </div>
);

class Beanboard extends Component {
    constructor() {
        super();
        this.state = {
            purchaseOrders: null,
            expenses: null,
            invoices: null,
            payments: null
        };
    }

    loadPurchaseOrders = () => {
        return new Promise(resolve => {
            setTimeout(() => resolve([
                'Tesla S',
            ]), 1000);
        }).then(purchaseOrders => this.setState({ purchaseOrders }));
    };

    loadExpenses = () => {
        return new Promise(resolve => {
            setTimeout(() => resolve([
                'Tesla S',
            ]), 1000);
        }).then(expenses => this.setState({ expenses }));
    };

    loadInvoices = () => {
        return new Promise(resolve => {
            setTimeout(() => resolve([
                'Tesla S',
            ]), 1000);
        }).then(invoices => this.setState({ invoices }));
    };

    loadPayments = () => {
        return new Promise(resolve => {
            setTimeout(() => resolve([
                'Tesla S',
            ]), 1000);
        }).then(payments => this.setState({ payments }));
    };

    render() {
        const { store } = this.props;

        return (
            <div className="app">
                <Header/>
                <div className="app-body">
                    <Sidebar { ...this.props }/>

                    <main className="main">
                        <Breadcrumb/>
                        <Container fluid>
                            <Switch>
                                <Route
                                    path="/beanboard"
                                    exact
                                    render={ (props) => (<UserPerformance store={ store } { ...props } />) }
                                />
                                <Route
                                    path="/beanboard/pos/create"
                                    exact
                                    render={ (props) => (<PurchaseOrdersCreate fetch={this.loadPurchaseOrders} list={this.state.purchaseOrders} store={ store } { ...props } />) }
                                />
                                <Route
                                    path="/beanboard/pos/inprogress"
                                    exact
                                    render={ (props) => (<PurchaseOrdersInProgress fetch={this.loadPurchaseOrders} list={this.state.purchaseOrders} store={ store } { ...props } />) }
                                />
                                <Route
                                    path="/beanboard/pos/approvals"
                                    exact
                                    render={ (props) => (<PurchaseOrdersApprovals fetch={this.loadPurchaseOrders} list={this.state.purchaseOrders} store={ store } { ...props } />) }
                                />
                                <Route
                                    path="/beanboard/pos/committed"
                                    exact
                                    render={ (props) => (<PurchaseOrdersCommitted fetch={this.loadPurchaseOrders} list={this.state.purchaseOrders} store={ store } { ...props } />) }
                                />
                                <Route
                                    path="/beanboard/pos/closed"
                                    exact
                                    render={ (props) => (<PurchaseOrdersClosed fetch={this.loadPurchaseOrders} list={this.state.purchaseOrders} store={ store } { ...props } />) }
                                />
                                <Redirect from="/beanboard/pos" to="/beanboard/pos/inprogress"/>
                                <Route
                                    path="/beanboard/expenses/receipts/arrivals"
                                    exact
                                    render={ (props) => (<ExpensesCreateReceipts fetch={this.loadExpenses} list={this.state.expenses} store={ store } { ...props } />) }
                                />
                                <Route
                                    path="/beanboard/expenses/create"
                                    exact
                                    render={ (props) => (<ExpensesCreateExpenses fetch={this.loadExpenses} list={this.state.expenses} store={ store } { ...props } />) }
                                />
                                <Route
                                    path="/beanboard/expenses/inprogress"
                                    exact
                                    render={ (props) => (<ExpensesInProgress fetch={this.loadExpenses} list={this.state.expenses} store={ store } { ...props } />) }
                                />
                                <Route
                                    path="/beanboard/expenses/approvals"
                                    exact
                                    render={ (props) => (<ExpensesApprovals fetch={this.loadExpenses} list={this.state.expenses} store={ store } { ...props } />) }
                                />
                                <Route
                                    path="/beanboard/expenses/committed"
                                    exact
                                    render={ (props) => (<ExpensesApproved fetch={this.loadExpenses} list={this.state.expenses} store={ store } { ...props } />) }
                                />
                                <Redirect from="/beanboard/expenses" to="/beanboard/expenses/receipts/arrivals"/>
                                <Route
                                    path="/beanboard/invoices/arrivals"
                                    exact
                                    render={ (props) => (<InvoicesCreate fetch={this.loadInvoices} list={this.state.invoices} store={ store } { ...props } />) }
                                />
                                <Route
                                    path="/beanboard/invoices/inprogress"
                                    exact
                                    render={ (props) => (<InvoicesInProgress fetch={this.loadInvoices} list={this.state.invoices} store={ store } { ...props } />) }
                                />
                                <Route
                                    path="/beanboard/invoices/approvals"
                                    exact
                                    render={ (props) => (<InvoicesApprovals fetch={this.loadInvoices} list={this.state.invoices} store={ store } { ...props } />) }
                                />
                                <Route
                                    path="/beanboard/invoices/export"
                                    exact
                                    render={ (props) => (<InvoicesExport fetch={this.loadInvoices} list={this.state.invoices} store={ store } { ...props } />) }
                                />
                                <Route
                                    path="/beanboard/invoices/archived"
                                    exact
                                    render={ (props) => (<InvoicesArchived fetch={this.loadInvoices} list={this.state.invoices} store={ store } { ...props } />) }
                                />
                                <Redirect from="/beanboard/invoices" to="/beanboard/invoices/inprogress"/>
                                <Route
                                    path="/beanboard/payments/create"
                                    exact
                                    render={ (props) => (<PaymentsCreate fetch={this.loadPayments} list={this.state.payments} store={ store } { ...props } />) }
                                />
                                <Route
                                    path="/beanboard/payments/inprogress"
                                    exact
                                    render={ (props) => (<PaymentsInProgress fetch={this.loadPayments} list={this.state.payments} store={ store } { ...props } />) }
                                />
                                <Route
                                    path="/beanboard/payments/approvals"
                                    exact
                                    render={ (props) => (<PaymentsApprovals fetch={this.loadPayments} list={this.state.payments} store={ store } { ...props } />) }
                                />
                                <Route
                                    path="/beanboard/payments/released"
                                    exact
                                    render={ (props) => (<PaymentsReleases fetch={this.loadPayments} list={this.state.payments} store={ store } { ...props } />) }
                                />
                                <Route
                                    path="/beanboard/payments/paid"
                                    exact
                                    render={ (props) => (<PaymentsPaid fetch={this.loadPayments} list={this.state.payments} store={ store } { ...props } />) }
                                />
                                <Route
                                    path="/beanboard/payments/archived"
                                    exact
                                    render={ (props) => (<PaymentsArchived fetch={this.loadPayments} list={this.state.payments} store={ store } { ...props } />) }
                                />
                                <Redirect from="/beanboard/payments" to="/beanboard/payments/inprogress"/>
                                <Route component={ NoMatch }/>
                            </Switch>
                        </Container>
                    </main>
                    <Aside/>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default connect(
    state => ({
        state
        // email: state.getIn(['auth', 'email']),
        // password: state.getIn(['auth', 'password']),
        // error: state.getIn(['auth', 'error']),
        // expiry: state.getIn(['session', 'expiry']),
        // token: state.getIn(['session', 'access_token']),
        // resetToken: state.getIn(['session', 'reset_token']),
        // rouid: state.getIn(['session', 'rootOrgUnit', 'id']),
        // xpod: state.getIn(['session', 'podNumber']),
        // session: state.getIn(['session']).toJS(),
    }),
    dispatch => {
        return {
            // onSubmit: data => dispatch(loginRequest(data.toJS())),
            // navigate: bindActionCreators(NavigationActions.navigate, dispatch),
            // setLoginEmail: bindActionCreators(setLoginEmail, dispatch),
            // setLoginPassword: bindActionCreators(setLoginPassword, dispatch),
            // initiateLogin: bindActionCreators(initiateLogin, dispatch),
            // autoLogin: bindActionCreators(autoLogin, dispatch),
        };
    }
)(Beanboard);
