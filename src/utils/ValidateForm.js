/**
 * Beanworks 2017
 *
 * @flow
 */

const ValidateForm = values => {
    // IMPORTANT: values is an Immutable.Map here!
    const errors = {};
    const isDev = process.env.NODE_ENV === 'development';

    if (!values.get('username')) {
        errors.username = 'Required';
    } else if (isDev ? (values.get('username').length < 1) : (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.get('username')))) {
        errors.username = 'Invalid username';
    }
    if (!values.get('password')) {
        errors.password = 'Required';
    } else if (values.get('password').length < 3 ) {
        errors.password = 'Must be 3 characters or more';
    }
    return errors;
};

export default ValidateForm;