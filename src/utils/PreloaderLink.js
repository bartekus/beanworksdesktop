import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { Badge, NavItem } from 'reactstrap';
import classNames from 'classnames';
import LoadingIndicator from '../components/LoadingIndicator'

const badge = (badge) => {
    if (badge) {
        const classes = classNames(badge.class);
        return (<Badge className={ classes } color={ badge.variant }>{ badge.text }</Badge>)
    }
};

class PreloaderLink extends Component {
    // static contextTypes = {
    //     history: PropTypes.shape({
    //         replace: PropTypes.func.isRequired,
    //         push: PropTypes.func.isRequired,
    //     }).isRequired
    // };

    constructor() {
        super();
        this.state = { loading: false };
    }

    handleClick = (evt) => {
        evt.preventDefault();
        this.setState({ loading: true });

        this.props.onPreload().then(() => {
            this.setState({ loading: false });

            const { replace, to } = this.props;

            if (replace) {
                this.props.history.replace(to)
            } else {
                this.props.history.push(to)
            }
        });
    };

    render() {
        const { index, navLinkClassName, name, item} = this.props;

        return name ?
                (<NavItem key={ index } onClick={ this.handleClick } style={ { backgroundColor: 'transparent !important' } }>
                    <NavLink key={ index } to={ item.url } className={ navLinkClassName } activeClassName="active">
                        <i className={ item.icon }> </i> { this.state.loading ? <LoadingIndicator /> : null } { name } { badge(item.badge) }
                    </NavLink>
                </NavItem>)
                :
            (<NavItem key={ index } onClick={ this.handleClick } style={ { backgroundColor: 'transparent !important' } }>
                <NavLink key={ index } to={ item.url } className={ navLinkClassName } activeClassName="active">
                   { this.state.loading ? <i className={ navLinkClassName }><LoadingIndicator /></i> :  <i className={ item.icon }>  </i>  } { badge(item.badge) }
                </NavLink>
            </NavItem>);
    }
}

export default PreloaderLink;
