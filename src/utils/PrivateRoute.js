/**
 * Beanworks 2017
 *
 * @flow
 */

import React from 'react';
import { Route, Redirect } from 'react-router';
import TimeUtil from './TimeUtil'

function checkAuthenticationToken(clientSessionExpiry) {
    return TimeUtil.dateIsInFuture(clientSessionExpiry);
}

const PrivateRoute = ({ component: Component, ...rest, store }) => {
    const fromStore = store.getState().toJS();
    const clientSessionExpiry = fromStore.client.expiry;
    const isAuthenticated = clientSessionExpiry ? checkAuthenticationToken(clientSessionExpiry) : null;

    return (<Route { ...rest } render={ (props) => {
        if (isAuthenticated) {
            return (<Component store={ store } { ...props } />)
        } else {
            return (<Redirect to={ {
                pathname: '/login',
                state: { from: props.location }
            } }/>)
        }
    }
    }/>)
};

export default PrivateRoute;
