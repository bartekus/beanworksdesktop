/**
 * Beanworks 2017
 *
 * @flow
 */

import moment from 'moment';

export default class TimeUtil {
    static DATE_TIME_API_FORMAT = 'YYYY-MM-DDTHH:mm:ssZ';
    static DATE_API_FORMAT = 'YYYY-MM-DD';
    static DATE_TIME_DISPLAY_FORMAT = 'DD-MMM-YY H:MM A';
    static DATE_TIME_DISPLAY_FORMAT_ALT = 'MMM-DD-YY H:MM A';
    static DATE_DISPLAY_FORMAT = 'MM-DD-YYYY';
    static DATE_DISPLAY_PARSE_ALT = 'MM-DD-YY';

    /** Validate moment Date object using a custom format. **/
    static customValidateDate(date: string, format: string): string {
        if (!date) {
            throw new Error(`Expected first arguments of TimeUtil.customValidateDate to be valid string, but got ${typeof date} ${date}`);
        }

        if (!format) {
            throw new Error(`Expected second arguments of TimeUtil.customValidateDate to be valid string, but got ${typeof format} ${format}`);
        }

        return moment(date, format, true).isValid();
    }

    /** Format Date object using a custom format. **/
    static customFormatDate(date: string, format: string): string {
        if (!date) {
            throw new Error(`Expected first arguments of TimeUtil.customFormatDate to be valid string, but got ${typeof date} ${date}`);
        }

        if (!format) {
            throw new Error(`Expected second arguments of TimeUtil.customFormatDate to be valid string, but got ${typeof format} ${format}`);
        }

        const formatDate = moment(date).format(format);
        const testDate =  moment(formatDate, format, true).isValid();

        if (!testDate) {
            throw new Error(`Invalid moment.js date: ${ date } or format: ${ format }.`);
        }

        return formatDate;
    }

    /** Format Date object to string containing date, time, and offset for api. **/
    static validateAPIDate(date: string) {
        return this.customValidateDate(date, this.DATE_TIME_API_FORMAT);
    }

    static formatAPIDateTime(date: string) {
        if (!date || !this.validateAPIDate(date)) {
            throw new Error('Invalid date object, cannot format');
        }

        return moment(date).format(this.DATE_TIME_API_FORMAT);
    }

    static formatAPIDate(date: string) {
        if (!date || !this.validateAPIDate(date)) {
            throw new Error('Invalid date object, cannot format');
        }

        return moment(date).format(this.DATE_API_FORMAT);
    }

    /** Parse DateTime string coming from the api **/
    static parseAPIDateTime(date: string) {
        if (!date || !this.customValidateDate(date, this.DATE_TIME_API_FORMAT)) {
            throw new Error('Invalid date object, cannot format');
        }

        return moment(moment(date).format(this.DATE_TIME_API_FORMAT));
    }

    static parseAPIDate(date: string) {
        if (!date || !this.customValidateDate(date, this.DATE_API_FORMAT)) {
            throw new Error('Invalid date object, cannot format');
        }

        return moment(moment(date).format(this.DATE_API_FORMAT));
    }

    static parseAPIDateOrDateTime(dateOrDateTimeString) {
        try {
            if (this.parseAPIDate(dateOrDateTimeString)) {
                return moment(this.parseAPIDate(dateOrDateTimeString));
            }
        } catch(error) {
            console.log('error', error); // eslint-disable-line
        }

        try {
            if (this.parseAPIDateTime(dateOrDateTimeString)) {
                return moment(this.parseAPIDateTime(dateOrDateTimeString));
            }
        } catch(error) {
            console.log('error', error); // eslint-disable-line
        }
    }

    /** Format date and time for display in the ui **/
    static formatDateTime(date) {
        const formatDate = moment(date).format(this.DATE_TIME_DISPLAY_FORMAT);
        const testDate =  moment(formatDate, this.DATE_TIME_DISPLAY_FORMAT, true).isValid();

        if (!testDate) {
            throw new Error('Invalid date object, cannot format');
        }

        return formatDate;
    }

    /** Format date and time for display in the ui **/
    static formatDateTimeAlt(date) {
        const formatDate = moment(date).format(this.DATE_TIME_DISPLAY_FORMAT_ALT);
        const testDate =  moment(formatDate, this.DATE_TIME_DISPLAY_FORMAT_ALT, true).isValid();

        if (!testDate) {
            throw new Error('Invalid date object, cannot format');
        }

        return formatDate;
    }

    /** Format date for display in the ui **/
    static formatDate(dt) {
        const formatDate = moment(dt).format(this.DATE_DISPLAY_FORMAT);
        const testDate =  moment(formatDate, this.DATE_DISPLAY_FORMAT, true).isValid();

        if (!testDate) {
            throw new Error('Invalid date object, cannot format');
        }

        return formatDate;
    }

    /** Parse string in the default ui format to date **/
    static validateDate(date) {
        return this.customValidateDate(date, this.DATE_DISPLAY_FORMAT);
    }

    static parseDate(date) {
        if (!date || !this.validateDate(date)) {
            throw new Error('Invalid date object, cannot format');
        }

        return moment(moment(date).format(this.DATE_DISPLAY_FORMAT));
    }

    /** Parse string in the default ui format to date **/
    static validateDateAlt(date) {
        return this.customValidateDate(date, this.DATE_DISPLAY_PARSE_ALT);
    }

    static parseDateAlt(date) {
        if (!date || !this.validateDateAlt(date)) {
            throw new Error('Invalid date object, cannot format');
        }

        return moment(moment(date).format(this.DATE_DISPLAY_PARSE_ALT));
    }

    /** Calculate the difference in days between two date objects. **/
    static dateDifferenceInDays(dt1, dt2) {
        const msPerDay = 24 * 60 * 60 * 1000;

        return Math.round((dt1.getTime() - dt2.getTime()) / msPerDay);
    }

    /** Compares the provided date to now returning true if the date is in future **/
    static dateIsInFuture(date) {
        return moment(this.formatAPIDateTime(date)).isAfter();
    }

}
