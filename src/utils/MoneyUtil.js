/**
 * Beanworks 2017
 *
 * @flow
 */

export default class MoneyUtil {
    static format(value: number, digits: number = 2): string {
        value = parseFloat(value);

        if (isNaN(value)) {
            throw new Error(`Expected first arguments of moneyUtil.format to be numeric, but got ${typeof value}`);
        }

        return `$${value.toFixed(digits)}`;
    }
}
