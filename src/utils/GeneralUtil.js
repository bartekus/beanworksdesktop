/**
 * Beanworks 2017
 *
 * @flow
 */

export default class GeneralUtil {
    static formatStatus(status: string): string {
        const array = status.split(/(?=[A-Z])/);
        return array
            .map((element: string): string => {
                return element;
            })
            .join(' ');
    }

    static capitalizeStatus(status: string): string {
        return status.replace(/\w/g, w => w.toUpperCase());
    }

    static formatAndCapitalizeStatus(status: string): string {
        return this.capitalizeStatus(this.formatStatus(status));
    }
}
