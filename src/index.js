/**
 * Beanworks 2017
 *
 * @flow
 */

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { IntlProvider } from 'react-intl-redux';
import { ConnectedRouter } from 'react-router-redux';
import { Route, Switch, Redirect } from 'react-router';
import Pace from 'pace-progress';

import Login from './views/pages/LoginContainer';
import PrivateRoute from './utils/PrivateRoute';
import Beanboard from './views/Beanboard';
// import Logout from './views/pages/Logout';
import Page404 from './views/pages/Page404';

import registerServiceWorker from './RegisterServiceWorker';
import configureStore from './redux/store';
import { history } from './redux/middleware/RouterMiddleware';

import 'font-awesome/css/font-awesome.min.css';
import 'simple-line-icons/css/simple-line-icons.css';
import './style.css'

require('./initLoader');

Pace.start();
async function init() {
    const store = await configureStore();
    const intlSelector = state => state.get('intl').toJS();

    ReactDOM.render(
        <Provider store={store}>
            <IntlProvider intlSelector={intlSelector}>
                <ConnectedRouter history={ history } >
                    <div>
                        <Switch>
                            <Redirect exact from="/" to="/beanboard"/>
                            <Route store={ store } path={ "/login" } component={ Login }/>
                            <PrivateRoute store={ store } path={ "/beanboard" } component={ Beanboard }/>
                            <Route store={ store } path={ "*" } component={ Page404 }/>
                        </Switch>
                    </div>
                </ConnectedRouter>
            </IntlProvider>
        </Provider>,
        document.getElementById('root')
    );
}
init();

registerServiceWorker();
