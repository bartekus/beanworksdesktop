## A case for Saga

Organizing side effects using redux-thunk might in principle give good result,
but the fact is that we still need to mock network requests in tests.
Basically as long as logic in the action creators or thunks is simple, everything is fine.
Unfortunately, for more than a few functions the code will become harder to read, which makes it even more difficult to test.

As alternative approach, instead of dispatching functions processed by redux-thunk,
we can create saga and write all the logic of event stream processing in a format that is more familiar to read and execute.
Unlike thunks that are carried out when you dispatch them,
sagas run in the background right after the app is launched and observe all actions that the store dispatches.

The concept of redux-saga is to create a background worker/thread for the app,
and it comes with lots of useful helper functions to handle tasks in a gradual and controlled manner.
It acts as a missing piece in react redux architecture given that actionCreators and reducers should be pure functions.

It allows us to write a complex sequence of synchronous and asynchronous events in a clear and declarative style without callbacks.
All while making application side effects easier to manage, more efficient to execute, simple to test, and better at handling failures.
It uses Generators which are kind of like async/await, but have few more features to make asynchronous flows easy to read, write and test. 
By doing so, it enables actions to stay pure and makes asynchronous flows appear like a standard synchronous JavaScript code which then can be easily tested.

Saga's generators pull the next action which mimicking thunk's pushing behavior, 
however unlike thunk which are called by the action creator on each new action,
(thus in effect end up pushing all the actions continually to thunks without any control on when to stop handling those actions)
saga can decide when to run and when not to run while also making it quite trivial to implement complex control flows that are cancelable,
can be ran conditionally or as needed, and can express concurrency requirement or an race condition.

Furthermore Sagas unlike thunks, can wait for an action/actions to be dispatched (`take`),
cancel existing routine (`cancel`, `takeLatest`, `race`),
offers channels to listen on external event sources (for example websockets)
and useful functionality, which generalize some common application patterns such as fork model (fork, spawn) and throttle
plus multiple routines can listen to the same action (take, takeEvery)

So in an essence the three key benefits of sagas are:
- Simplicity as Saga's offer independent place to handle and organizing all side effects sequences and business logic using generators
- Declarative style with emphasis on pure functions that's easy to read
- Simplicity and Testability as sagas return pure objects

## Saga put into practice

> An example of a simple saga which sends a network request:

```js
function* request(action) {
   const response = yield call(api.method, action.payload)
   if (response.token) {
     yield put({type: ‘API_CALL_SUCCESS’, payload: response.token});
   } else {
     yield put({type: ‘API_CALL_FAIL’})
   }
}

function* apiSaga() {
   takeEvery(‘API_CALL’, request)
}

```

Everything is quite simple and clear here.
Saga or generator expects some action and after receiving it, runs the handler function.
To make this code clearer and simpler, it possible to simplify the code more:

```js
function* apiSaga() {
   while ( true ) {
   yield take(‘API_CALL’);
   const response = yield call(api.method, action.payload)
   
   if (response.token) {
     yield put({type: ‘API_CALL_SUCCESS’, payload: response.token});
   } else {
     yield put({type: ‘API_CALL_FAIL’})
   }
}
```

So now we have launched an endless loop and as a result,
the saga stops at the instructions “yield take ( ‘API_CALL’)” and waits for the desired action.

Special functions or effects control sagas and these functions simply return an object that give a description of the desired call.
Since all calls in the saga are indirect (through effects), it’s easy to test them:

```js
if(‘should handle API_CALL type and perform api.method’, () => {
   const saga = mySaga();
   let next = saga.next();
   
   expect(next.value).toEqual(take(‘API_CALL’}));
   next = saga.next({type: ‘API_CALL’, payload: 1});
   expect(next.value).toEqual(call(api.method, {payload: 1}));
   next = saga.next({token: 1});
   expect(next.value).toEqual(put({type: ‘API_CALL_SUCCESS’}, payload: 1}))
});

```

Here is an excellent article why [redux-saga is a good idea in react-native apps](https://shift.infinite.red/using-redux-saga-to-simplify-your-growing-react-native-codebase-2b8036f650de)
